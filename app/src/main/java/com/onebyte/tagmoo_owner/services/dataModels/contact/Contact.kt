package com.onebyte.tagmoo_owner.services.dataModels.contact


data class Contact(
    val phone: String = "",
    val email: String = "",
    val cnic: String = "",
    val message: String = "",
    val subject: String = ""
)