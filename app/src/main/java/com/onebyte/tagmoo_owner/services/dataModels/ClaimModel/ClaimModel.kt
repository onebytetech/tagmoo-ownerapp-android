package com.onebyte.tagmoo_owner.services.dataModels.ClaimModel

data class ClaimModel(
    val body: Body = Body(),
    val status: Status = Status()
)