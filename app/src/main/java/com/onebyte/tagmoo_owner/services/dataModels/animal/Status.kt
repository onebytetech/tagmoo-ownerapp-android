package com.onebyte.tagmoo_owner.services.dataModels.animal

data class Status(
    val code: Int = 0,
    val message: String = ""
)