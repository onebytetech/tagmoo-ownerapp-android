package com.onebyte.tagmoo_owner.services.dataModels.animal

data class RegisterPolicy(
    val _id: String = "",
    val policyStatus: String = "",
    var ownerId: String = "",
    val animalId: String = "",
    val receiptNumber: String = "",
    var insuredAmount: String = "",
    var policyStartDate: String = "",
    var createdAt: String = "",
    val updatedAt: String = "",
    val status: String = "",
    val netPremium: String = "",
    var product: PolicyId? = null,
    val tenor: String = ""
)