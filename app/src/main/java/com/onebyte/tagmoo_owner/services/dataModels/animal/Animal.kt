package com.onebyte.tagmoo_owner.services.dataModels.animal

data class Animal(
    val __v: Int = 0,
    val _id: String = "",
    var age: Int = 0,
    val createdAt: String = "",
    var immunizationStatus: String = "",
    var location: Location = Location(),
    val muzzleCount: Int = 0,
    var name: String = "",
    val ownerId: String = "",
    val registeredPolicyId: String = "",
    var sex: String = "",
    var status: String = "",
    var type: String = "",
    var category: String = "",
    val updatedAt: String = "",
    var weight: String = "",
    var claimStatus: String = "",
    var profilePic: String = "",
    var latitude: Double = 0.0,
    var longitude: Double= 0.0
)