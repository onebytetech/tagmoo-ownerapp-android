package com.onebyte.tagmoo_owner.services.dataModels.uploadImageModels

import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status

class UploadImageModel(
    var status: Status? = null,
    var body: UploadImage? = null
)
