package com.onebyte.tagmoo_owner.services.dataModels.contact

import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status


data class ContactModel(
    val status: Status,
    val body: String = ""
)