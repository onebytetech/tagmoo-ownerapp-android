package com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels

data class AddedAnimal(
    var id: Long,
    var animal: String,
    var isUploaded: Boolean,
    var isUploading: Boolean,
    var countUploaded: Int,
    var animalList: List<AnimalImages>
)