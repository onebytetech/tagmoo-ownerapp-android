package com.onebyte.tagmoo_owner.services.dataModels.animal

data class PolicyId(
    var branchName: String = "",
    var insuranceCompanyName: String = "",
    var productPricing: String = "",
    var productName: String = "",
    var totalNoOfTotalPremiumValue: String = "",
    var _id: String = ""
)