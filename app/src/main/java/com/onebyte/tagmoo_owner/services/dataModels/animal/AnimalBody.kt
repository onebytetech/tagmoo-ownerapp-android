package com.onebyte.tagmoo_owner.services.dataModels.animal

data class AnimalBody(
    val animalIds: List<Animal> = listOf()
)