package com.onebyte.tagmoo_owner.services.dataModels.otp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status

data class OtpResponse(
    val status: Status,
    val body: String
)