package com.onebyte.tagmoo_owner.services.dataModels.ClaimModel

data class Claim(
    var animalId: String = "",
    var ownerId: String = "",
    var reason: String = "",
    var registeredPolicyId: String = "",
    var mlResult: String = "",
    var proofDocs: List<ProofDocs>? = null
)