package com.onebyte.tagmoo_owner.services.dataModels.userOwner

data class UserModel(
    val body: Body,
    val status: Status
)