package com.onebyte.tagmoo_owner.services.dataModels.animal

data class Body(
    val animal: Animal = Animal(),
    val animalId: String? = null,
    val confidence_level: Double = 0.0,
    val `return`: String = ""
)