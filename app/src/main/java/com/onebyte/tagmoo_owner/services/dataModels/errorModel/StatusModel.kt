package com.onebyte.tagmoo_owner.services.dataModels.errorModel

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class StatusModel(
    val status: Status?
)