package com.onebyte.tagmoo_owner.services.networkServices

import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.Claim
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ClaimModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalCompare
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalDetailModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalModels
import com.onebyte.tagmoo_owner.services.dataModels.contact.Contact
import com.onebyte.tagmoo_owner.services.dataModels.contact.ContactModel
import com.onebyte.tagmoo_owner.services.dataModels.otp.OtpResponse
import com.onebyte.tagmoo_owner.services.dataModels.otp.SendOtp
import com.onebyte.tagmoo_owner.services.dataModels.otp.VerifyOtp
import com.onebyte.tagmoo_owner.services.dataModels.uploadImageModels.UploadImageModel
import com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels.UploadPictureResponse
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.Owner
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.OwnerModel
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.User
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface NetworkService {
    @Multipart
    @POST("app/image-ml/upload-img")
    suspend fun uploadImage(@Part body: MultipartBody.Part, @Part("body") animal1: Animal,
                            @Query("animalId") animal: String,
                            @Query("ownerId") ownerId: String,
                            @Header("Authorization") toke: String): Response<UploadPictureResponse>

    @Multipart
    @POST("app/image-ml/compare-img")
    suspend fun uploadImage(@Part body: MultipartBody.Part, @Query("ownerId") ownerId: String,
                            @Header("Authorization") toke: String ): Response<AnimalCompare>

    @Multipart
    @POST("upload")
    suspend fun uploadImage(@Part body: MultipartBody.Part, @Header("Authorization") toke: String ): Response<UploadImageModel>

    @POST("users/sign-in")
    suspend fun loginUser(@Body user: User): Response<UserModel>

    @GET("owner/get-animals")
    suspend fun getAnimals(@Header("Authorization") toke: String): Response<AnimalModels>

    @GET("owner/search")
    suspend fun getAnimalsSearch(@Header("Authorization") toke: String, @Query("txt") txt: String): Response<AnimalModels>

    @GET("owner/filter")
    suspend fun getAnimals(@Header("Authorization") toke: String, @Query("tag") filter: String): Response<AnimalModels>

    @GET("owner/get-animals")
    suspend fun getAnimal(@Header("Authorization") toke: String, @Query("animalId") animalId: String): Response<AnimalDetailModel>

    @PUT("owner/cancel-lodged-claim")
    suspend fun cancelClaim(@Header("Authorization") toke: String, @Query("claimId") claimId: String): Response<ClaimModel>

    @GET("sub-agent/get-owners")
    suspend fun getSubAgentOwners(@Header("Authorization") toke: String): Response<OwnerModel>

    @POST("users/create-profile")
    suspend fun addOwner(@Body user: Owner, @Header("Authorization") toke: String): Response<UserModel>

    @POST("users/send-otp")
    suspend fun sendOtp(@Body sendOtp: SendOtp): Response<OtpResponse>

    @POST("users/verify-otp")
    suspend fun verifyOtp(@Body verifyOtp: VerifyOtp, @Header("Authorization") toke: String):Response<UserModel>

    @POST("users/contact-tagmoo")
    suspend fun contact(@Body contact: Contact, @Header("Authorization") toke: String): Response<ContactModel>

    @POST("owner/lodge-claim")
    suspend fun fileClaim(@Body user: Claim, @Header("Authorization") toke: String): Response<ClaimModel>
}