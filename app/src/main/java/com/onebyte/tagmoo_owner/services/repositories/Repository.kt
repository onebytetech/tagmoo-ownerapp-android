package com.onebyte.tagmoo_owner.services.repositories

import android.content.SharedPreferences
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.Claim
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ClaimModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalCompare
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalDetailModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalModels
import com.onebyte.tagmoo_owner.services.dataModels.contact.Contact
import com.onebyte.tagmoo_owner.services.dataModels.contact.ContactModel
import com.onebyte.tagmoo_owner.services.dataModels.otp.OtpResponse
import com.onebyte.tagmoo_owner.services.dataModels.otp.SendOtp
import com.onebyte.tagmoo_owner.services.dataModels.otp.VerifyOtp
import com.onebyte.tagmoo_owner.services.dataModels.uploadImageModels.UploadImageModel
import com.onebyte.tagmoo_owner.services.networkServices.NetworkService
import com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels.UploadPictureResponse
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.Owner
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.OwnerModel
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.User
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.PrefManager
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject

class Repository @Inject constructor(private val networkService: NetworkService, private val sp: SharedPreferences) {
    suspend fun uploadImage(imageUrl: String, ownerId: String  , isCompare: Boolean, token: String): Response<UploadPictureResponse> {
        val file = File(imageUrl)
        val requestFile = file.asRequestBody("image/*".toMediaTypeOrNull())
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)

        val animal = Animal()
        animal.immunizationStatus = "not-immunized"

        return networkService.uploadImage(body,animal,  Constants.animal, ownerId, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun uploadImage(imageUrl: String): Response<UploadImageModel> {
        val file = File(imageUrl)
        val requestFile = file.asRequestBody("image/*".toMediaTypeOrNull())
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
        return networkService.uploadImage(body, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun uploadImageToCompare(imageUrl: String, ownerId: String  , isCompare: Boolean, token: String): Response<AnimalCompare> {
        val file = File(imageUrl)
        val requestFile = file.asRequestBody("image/*".toMediaTypeOrNull())
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
        return networkService.uploadImage(body, ownerId, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun loginUser(user: User): Response<UserModel>{
        return networkService.loginUser(user)
    }

    suspend fun addOwner(user: Owner, token: String): Response<UserModel>{
        return networkService.addOwner(user, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun contact(contact: Contact): Response<ContactModel>{
        return networkService.contact(contact, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun getAnimals() : Response<AnimalModels>{
        return  networkService.getAnimals(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun getAnimals(isSearch: Boolean, filter: String) : Response<AnimalModels>{
        return if(isSearch)
            networkService.getAnimalsSearch(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!, filter)
        else
            networkService.getAnimals(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!, filter)
    }

    suspend fun getSubAgentOwners(token: String) : Response<OwnerModel>{
        return networkService.getSubAgentOwners(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun fileClaim(claim: Claim): Response<ClaimModel>{
        return networkService.fileClaim(claim, PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
    }

    suspend fun getAnimal(animalId: String): Response<AnimalDetailModel>{
        return networkService.getAnimal(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!, animalId)
    }

    suspend fun cancelClaim(claimId: String): Response<ClaimModel>{
        return networkService.cancelClaim(PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!, claimId)
    }

    suspend fun verifyOtp(verifyOtp: VerifyOtp, token: String) : Response<UserModel>{
        return networkService.verifyOtp(verifyOtp, "Bearer $token")
    }

    suspend fun sendOtp(sendOtp: SendOtp) : Response<OtpResponse> {
        return networkService.sendOtp(sendOtp)
    }
}