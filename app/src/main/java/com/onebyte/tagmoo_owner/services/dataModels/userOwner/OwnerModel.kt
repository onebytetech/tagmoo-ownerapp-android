package com.onebyte.tagmoo_owner.services.dataModels.userOwner

data class OwnerModel(
    val body: List<Body> = listOf(),
    val status: Status
)