package com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels

data class AnimalImages(
    var id: Int,
    var url: String,
    var isSuccess: Boolean,
    var isUploaded: Boolean
)