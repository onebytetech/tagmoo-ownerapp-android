package com.onebyte.tagmoo_owner.services.dataModels.otp

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class SendOtp(
    val phoneNumber: String
)