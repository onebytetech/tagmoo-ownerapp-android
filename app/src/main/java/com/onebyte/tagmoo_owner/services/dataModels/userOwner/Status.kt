package com.onebyte.tagmoo_owner.services.dataModels.userOwner

data class Status(
    val code: Int,
    val message: String
)