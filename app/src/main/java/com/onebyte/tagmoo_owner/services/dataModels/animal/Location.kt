package com.onebyte.tagmoo_owner.services.dataModels.animal

data class Location(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0
)