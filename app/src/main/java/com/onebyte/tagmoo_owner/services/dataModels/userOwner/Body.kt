package com.onebyte.tagmoo_owner.services.dataModels.userOwner


data class Body(
    val user: User,
    val body: List<User> = listOf(),
    val __v: Int = 0,
    val _id: String = "",
    var address: String = "",
    val agentIds: MutableList<Any> = mutableListOf(),
    val animalIds: MutableList<Any> = mutableListOf(),
    var cnic: String = "",
    val commissionAmount: Int = 0,
    val createdAt: String = "",
    val deleted: Boolean = false,
    var dob: String = "",
    val duePayables: Int = 0,
    val earnings: Int = 0,
    var email: String = "",
    var fatherName: String = "",
    var firstName: String = "",
    var lastName: String = "",
    val occupation: String = "",
    val ownerIds: MutableList<Any> = mutableListOf(),
    var password: String = "",
    var phoneNumber: String = "",
    val profilePic: String = "",
    val receivedAmount: Int = 0,
    var role: String = "owner",
    val status: String = "active",
    val subagentIds: MutableList<Any> = mutableListOf(),
    val token: String = "",
    val updatedAt: String = ""
)