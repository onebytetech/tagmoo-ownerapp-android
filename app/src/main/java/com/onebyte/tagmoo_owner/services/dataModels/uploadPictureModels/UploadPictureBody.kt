package com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels

data class UploadPictureBody(
    var image: String,
    var animal: String,
    var animalId: String,
    var confidence_level: String
)