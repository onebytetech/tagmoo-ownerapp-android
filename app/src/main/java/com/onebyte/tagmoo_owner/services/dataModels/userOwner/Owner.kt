package com.onebyte.tagmoo_owner.services.dataModels.userOwner

data class Owner(

    var address: String = "",
    var cnic: String = "",
    var dob: String = "",
    var email: String = "",
    var fatherName: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var password: String = "",
    var phoneNumber: String = "",
    val profilePic: String = "",
    var role: String = "owner"

){
}