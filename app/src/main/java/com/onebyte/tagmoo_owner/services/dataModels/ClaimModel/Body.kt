package com.onebyte.tagmoo_owner.services.dataModels.ClaimModel

data class Body(
    var __v: Int = 0,
    var _id: String = "",
    var animalId: String = "",
    var createdAt: String = "",
    var ownerId: String = "",
    var proofDocs: List<Any> = listOf(),
    var reason: String = "",
    var registeredPolicyId: String = "",
    var status: String = "",
    var updatedAt: String = ""
)