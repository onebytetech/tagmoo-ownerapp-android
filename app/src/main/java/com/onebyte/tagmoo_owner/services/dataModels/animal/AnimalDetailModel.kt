package com.onebyte.tagmoo_owner.services.dataModels.animal

data class AnimalDetailModel(
   var status: Status? = null,
   var body: AnimalDetail? = null
)