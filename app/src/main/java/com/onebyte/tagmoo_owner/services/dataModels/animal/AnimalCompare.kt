package com.onebyte.tagmoo_owner.services.dataModels.animal

import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status

data class AnimalCompare(
    var status: Status,
    val body: Body = Body()
)