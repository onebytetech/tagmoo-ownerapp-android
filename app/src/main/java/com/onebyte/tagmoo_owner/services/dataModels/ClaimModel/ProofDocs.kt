package com.onebyte.tagmoo_owner.services.dataModels.ClaimModel

data class ProofDocs(
    var title: String = "",
    var docUrl: String = "",
    var description: String = ""
)