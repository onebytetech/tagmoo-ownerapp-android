package com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels

import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status

data class UploadPictureResponse(
    var status: Status,
    var body: UploadPictureBody
)