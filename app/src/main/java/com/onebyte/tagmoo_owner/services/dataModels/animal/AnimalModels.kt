package com.onebyte.tagmoo_owner.services.dataModels.animal

data class AnimalModels(
    val body: AnimalBody? = null,
    val status: Status = Status()
)