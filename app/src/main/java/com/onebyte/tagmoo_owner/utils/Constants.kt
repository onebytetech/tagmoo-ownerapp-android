package com.onebyte.tagmoo_owner.utils

object Constants {

    object PreferenceKeys {
        var imageUrl = "TEMP_IMAGE"
        const val compare = "COMPARE"
        const val IMAGE_URLS = "IMAGE_URLS"
        const val ANIMALS = "ANIMALS"
        const val POLICY = "POLICY"
        const val COUNT = "COUNT"
        const val TOKEN = "TOKEN"
        const val USER = "USER"
        const val LOCALE = "LOCALE"

    }

    object CameraKeys{
        var SUPPORTING_DOCS = "SUPPORTING_DOCS"
        var CUT_HEAD = "CUT_HEAD"
        var DEAD_ANIMAL = "DEAD_ANIMAL"
        var DEATH_CERTIFICATE = "DEATH_CERTIFICATE"
        var PROFILE_PIC = "PROFILE_PIC"
    }

    object FilterKeys {
        const val PENDING = "pending"
        const val CLAIMED = "claims"
        const val INCOMPLETE = "incomplete"
    }

    object NavigationKeys {
        const val ownerId = "OWNERID"
        const val animalId = "ANIMAL_ID"
    }

    var baseUrl = "https://api.tagmu.co/api/v1/"
//        var baseUrl = "https://sapi.tagmu.co/api/v1/"

    var animal = ""
}