package com.onebyte.tagmoo_owner.utils

import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.User
import com.onebyte.tagmoo_owner.utils.Constants.PreferenceKeys.ANIMALS
import com.onebyte.tagmoo_owner.utils.Constants.PreferenceKeys.IMAGE_URLS
import com.onebyte.tagmoo_owner.utils.Constants.PreferenceKeys.LOCALE
import com.onebyte.tagmoo_owner.utils.Constants.PreferenceKeys.USER
import java.lang.Exception

class PrefManager {

    companion object {
        /**
         * Save date in preferences
         */
        fun savePreferences(key: String, value: String, sp: SharedPreferences) {
            val edit = sp.edit()
            edit.putString(key, value)
            edit.apply()
        }

        /**
         * Save urls in preferences
         */
        fun saveListImageUrlsPreferences(value: ArrayList<String>, sp: SharedPreferences) {
            val list = Gson().toJson(value)
            val edit = sp.edit()
            edit.putString(IMAGE_URLS, list)
            edit.apply()
        }

        /**
         * Save user in preferences
         */
        fun saveUser(user: User, sp: SharedPreferences) {
            val list = Gson().toJson(user)
            val edit = sp.edit()
            edit.putString(USER, list)
            edit.apply()
        }

        /**
         * Save Locale in preferences
         */
        fun saveLocale(value: String, sp: SharedPreferences) {
            val edit = sp.edit()
            edit.putString(LOCALE, value)
            edit.apply()
        }

        /**
         * Get Locale in preferences
         */
        fun getLocale(sp: SharedPreferences): String {
            return loadPreferences(sp, LOCALE)!!
        }

        /**
         * Save urls in preferences
         */
        fun saveNewAnimalListPreferences(value: AddedAnimal, sp: SharedPreferences, pos: Int) {
            val animal = loadNewAnimalListPreferences(sp)
            if (pos == -1) {
                (animal as ArrayList<AddedAnimal>).add(value)
            } else {
                ((animal as ArrayList<AddedAnimal>)[animal.size - 1].animalList as ArrayList<AnimalImages>).addAll(
                    value.animalList
                )
            }
            val listStr = Gson().toJson(animal)
            val edit = sp.edit()
            edit.putString(ANIMALS, listStr)
            edit.apply()
        }

        /**
         * Save urls in preferences
         */
        fun updateAnimalListPreferences(
            value: AddedAnimal,
            sp: SharedPreferences,
            pos: Int
        ): List<AddedAnimal> {
            val animal = loadNewAnimalListPreferences(sp)
            (animal as ArrayList<AddedAnimal>)[pos] = value
            val listStr = Gson().toJson(animal)
            val edit = sp.edit()
            edit.putString(ANIMALS, listStr)
            edit.apply()
            return animal
        }

        /**
         * Get urls from preferences
         */
        fun loadListImageUrlsPreferences(sp: SharedPreferences): ArrayList<String> {
            try {
                val jsonUrls = loadPreferences(sp, IMAGE_URLS)
                val gson = Gson()
                val type = object : TypeToken<List<String>>() {}.type
                return gson.fromJson<ArrayList<String>>(jsonUrls, type)
            } catch (ex: Exception) {
                Log.e("conversionListError", "$ex")
                return ArrayList()
            }
        }

        /**
         * Get urls from preferences
         */
        fun loadNewAnimalListPreferences(sp: SharedPreferences): List<AddedAnimal> {
            try {
                val jsonUrls = loadPreferences(sp, ANIMALS)
                val gson = Gson()
                val type = object : TypeToken<List<AddedAnimal>>() {}.type
                return gson.fromJson<List<AddedAnimal>>(jsonUrls, type)
            } catch (ex: Exception) {
                Log.e("conversionListError", "$ex")
                return arrayListOf()
            }
        }

        /**
         * Get user from preferences
         */
        fun getUser(sp: SharedPreferences): User? {
            val jsonUrls = loadPreferences(sp, USER)
            if(!jsonUrls.equals("0")) {
                val gson = Gson()
                return gson.fromJson<User>(jsonUrls, User::class.java)
            } else
                return null
        }

        /**
         * Save boolean date in preferences
         */
        fun saveBooleanPreferences(key: String, value: Boolean, sp: SharedPreferences) {
            val edit = sp.edit()
            edit.putBoolean(key, value)
            edit.apply()
        }

        /**
         * Load date in preferences
         */
        fun loadPreferences(sp: SharedPreferences, Key: String): String? {
            return sp.getString(Key, "0")

        }

        /**
         * Load boolean date in preferences
         */
        fun loadBooleanPreferences(sp: SharedPreferences, Key: String): Boolean {
            return sp.getBoolean(Key, false)

        }

        /**
         * Remove specific date from preferences
         */
        fun removePrefrence(sp: SharedPreferences, key: String) {
            val editor = sp.edit()
            editor.remove(key)
            editor.apply()
        }
    }

}