package com.onebyte.tagmoo_owner.utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import com.onebyte.tagmoo_owner.R
import java.lang.NullPointerException


@Suppress("IfThenToSafeAccess")
class CustomPopups {

    private lateinit var onClick: OnClick
    private lateinit var onClickConfirm: OnClickConfirm

    fun showAlertPopup(title: String, message: String, activity: Activity, onClick: OnClick?) {
        if(onClick != null)
            this.onClick = onClick
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_alert_puopup)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = dialog.window
        window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        window.setWindowAnimations(R.style.FadeInOutDialogAnimation)
        dialog.show()

        var msg = message
        if(msg.contains("Failed to connect") || msg.equals("Software caused connection abort"))
            msg = "No Internet Connection"
        else if(msg.contains("unexpected end of stream"))
            msg = "Failed to upload"

        dialog.findViewById<TextView>(R.id.tvDetail).text = msg
        dialog.findViewById<TextView>(R.id.tvHeading).text = title

        dialog.findViewById<View>(R.id.tvOk)
            .setOnClickListener {
                try {
                    dialog.dismiss()
                    if(onClick != null)
                        onClick.onBackPress()
                } catch (ex: NullPointerException){
                    Log.e("PopupClickException","$ex")
                }
            }
    }

    fun showConfirmPopup(title: String, message: String, activity: Activity, onClickConfirm: OnClickConfirm?) {
        if(onClickConfirm != null)
            this.onClickConfirm = onClickConfirm
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.layout_confirm_puopup)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val window = dialog.window
        window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        window.setWindowAnimations(R.style.FadeInOutDialogAnimation)
        dialog.show()

        dialog.findViewById<TextView>(R.id.tvDetail).text = message
        dialog.findViewById<TextView>(R.id.tvHeading).text = title

        dialog.findViewById<View>(R.id.tvOk)
            .setOnClickListener {
                try {
                    dialog.dismiss()
                    if(onClickConfirm != null)
                        onClickConfirm.onBackPress(true)
                } catch (ex: NullPointerException){
                    Log.e("PopupClickException","$ex")
                }
            }

        dialog.findViewById<View>(R.id.tvCancel)
            .setOnClickListener {
                try {
                    dialog.dismiss()
                    if(onClickConfirm != null)
                        onClickConfirm.onBackPress(false)
                } catch (ex: NullPointerException){
                    Log.e("PopupClickException","$ex")
                }
            }
    }
    interface OnClick {
        fun onBackPress()
    }

    interface OnClickConfirm {
        fun onBackPress(isYes: Boolean)
    }
}
