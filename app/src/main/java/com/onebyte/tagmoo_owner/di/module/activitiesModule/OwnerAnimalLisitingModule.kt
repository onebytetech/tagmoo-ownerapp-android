package com.onebyte.tagmoo_owner.di.module.activitiesModule

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebyte.tagmoo_owner.ui.home.OwnerHomeActivity
import com.onebyte.tagmoo_owner.ui.home.fragments.AnimalImagesAdapter
import com.onebyte.tagmoo_owner.ui.home.fragments.AnimalListAdapter
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class OwnerAnimalLisitingModule {

    @Provides
    fun provideProgressHUD(animalImagesAdapter: OwnerHomeActivity): ProgressHUD {
        return ProgressHUD(animalImagesAdapter)
    }
    @Provides
    fun provideAddedAnimalAdapter(animalImagesAdapter: AnimalImagesAdapter): AnimalListAdapter {
        return AnimalListAdapter(animalImagesAdapter)
    }

    @Provides
    fun provideAnimalImagesAdapter(): AnimalImagesAdapter {
        return AnimalImagesAdapter()
    }

    @Provides
    fun provideAddedAnimalLayoutManager(context: Context): LinearLayoutManager{
        return LinearLayoutManager(context)
    }
}