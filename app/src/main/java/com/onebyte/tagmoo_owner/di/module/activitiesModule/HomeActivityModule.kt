package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.ui.home.fragments.*
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class HomeActivityModule {

    @Provides
    fun provideLoader(homeActivity: HomeActivity): ProgressHUD {
        return ProgressHUD(homeActivity)
    }

    @Provides
    fun provideAddedAnimalFragment(): AddedAnimalFragment {
        return AddedAnimalFragment()
    }

    @Provides
    fun provideVerifyAnimalFragment(): VerifyAnimalActivity {
        return VerifyAnimalActivity()
    }

    @Provides
    fun provideHomeFragment(): HomeFragment {
        return HomeFragment()
    }

    @Provides
    fun provideListingFragment(): ListingFragment {
        return ListingFragment()
    }

    @Provides
    fun provideFinancialFragment(): FinancialFragment {
        return FinancialFragment()
    }

    @Provides
    fun provideSettingsFragment(): SettingsFragment {
        return SettingsFragment()
    }
}