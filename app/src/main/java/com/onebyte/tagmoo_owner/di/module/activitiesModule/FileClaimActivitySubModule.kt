package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class FileClaimActivitySubModule {

    @Provides
    fun provideLoader(fileClaim: VerifyAnimalActivity): ProgressHUD {
        return ProgressHUD(fileClaim)
    }

}