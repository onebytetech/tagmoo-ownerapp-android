package com.onebyte.tagmoo_owner.di.module

import com.onebyte.tagmoo_owner.di.module.fragmentModule.AddedAnimalFragmentModule
import com.onebyte.tagmoo_owner.di.module.fragmentModule.ListinglFragmentModule
import com.onebyte.tagmoo_owner.ui.add.fragments.AddAnimalFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.AddPolicyFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CameraFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CapturedImagesFragment
import com.onebyte.tagmoo_owner.ui.home.fileClaim.FileClaimCameraFragment
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalFragment
import com.onebyte.tagmoo_owner.ui.home.fragments.*
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {

    @ContributesAndroidInjector(modules = [AddedAnimalFragmentModule::class])
    internal abstract fun contributeAddedAnimalFragment(): AddedAnimalFragment

//    @ContributesAndroidInjector
//    internal abstract fun contributeVerifyAnimalFragment(): VerifyAnimalActivity

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [ListinglFragmentModule::class])
    internal abstract fun contributeListingFragment(): ListingFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFinancialFragment(): FinancialFragment

    @ContributesAndroidInjector
    internal abstract fun contributeSettingsFragment(): SettingsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCameraFragment(): CameraFragment

    @ContributesAndroidInjector
    internal abstract fun contributeAddAnimalFragment(): AddAnimalFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCapturedImagesFragment(): CapturedImagesFragment

    @ContributesAndroidInjector
    internal abstract fun contributeAddPolicyFragment(): AddPolicyFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFileClaimCameraFragment(): FileClaimCameraFragment

    @ContributesAndroidInjector
    internal abstract fun contributeVerifyAnimalFragment(): VerifyAnimalFragment
}