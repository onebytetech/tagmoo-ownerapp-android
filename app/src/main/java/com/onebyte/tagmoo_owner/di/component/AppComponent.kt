package com.onebyte.tagmoo_owner.di.component

import android.app.Application
import com.onebyte.tagmoo_owner.application.MyApplication
import com.onebyte.tagmoo_owner.di.assistedFactory.AssistedInjectionFactory
import com.onebyte.tagmoo_owner.di.assistedFactory.ValidatorAssistedInjectionFactory
import com.onebyte.tagmoo_owner.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton
import dagger.android.support.AndroidSupportInjectionModule
import com.onebyte.tagmoo_owner.di.module.AppModule

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        AssistedInjectionModule::class,
        ViewModelModule::class,
        ActivitiesModule::class,
        FragmentsModule::class,
        NetworkServiceModule::class,
        NetworkRequestModule::class,
        RepositoriesModule::class,
        CustomPopupModule::class
    ]
)
interface AppComponent : AndroidInjector<MyApplication> {

    override fun inject(instance: MyApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    val assistedInjectionFactory: AssistedInjectionFactory.Factory
    val validatorAssistedInjectionFactory: ValidatorAssistedInjectionFactory.Factory
}