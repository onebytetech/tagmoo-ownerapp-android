package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides
import com.onebyte.tagmoo_owner.ui.contact.ContactActivity

@Module
class ContactActivityModule {

    @Provides
    fun provideLoader(homeActivity: ContactActivity): ProgressHUD {
        return ProgressHUD(homeActivity)
    }
}