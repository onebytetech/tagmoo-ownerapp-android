package com.onebyte.tagmoo_owner.di.module

import android.app.Dialog
import com.onebyte.tagmoo_owner.ui.takePictureView.TakePictureActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ActivityModule {

    @Provides
    @Singleton
    fun provideDailog(takePictureActivity : TakePictureActivity): Dialog {
        return ProgressHUD(takePictureActivity)
    }
}