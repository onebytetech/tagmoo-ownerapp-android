package com.onebyte.tagmoo_owner.di.module

import com.onebyte.tagmoo_owner.ui.contact.ContactActivity
import com.onebyte.tagmoo_owner.di.module.activitiesModule.*
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddClientActivity
import com.onebyte.tagmoo_owner.ui.cameraView.CameraActivity
import com.onebyte.tagmoo_owner.ui.detail.PolicyDetailActivity
import com.onebyte.tagmoo_owner.ui.forgotPassword.ForgotPasswordActivity
import com.onebyte.tagmoo_owner.ui.home.AnimalDetailActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.OwnerHomeActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.ui.login.LanguageActivity
import com.onebyte.tagmoo_owner.ui.login.LoginActivity
import com.onebyte.tagmoo_owner.ui.login.OtpActivity
import com.onebyte.tagmoo_owner.ui.login.OwnerLoginActivity
import com.onebyte.tagmoo_owner.ui.takePictureView.AllPictureActivity
import com.onebyte.tagmoo_owner.ui.takePictureView.TakePictureActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    internal abstract fun contributeHomeActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [LoginActivitySubModule::class])
    internal abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector(modules = [OwnerLoginActivitySubModule::class])
    internal abstract fun contributeOwnerLoginActivity(): OwnerLoginActivity

    @ContributesAndroidInjector(modules = [OwnerAnimalLisitingModule::class])
    internal abstract fun contributeOwnerAnimalListingActivity(): OwnerHomeActivity


    @ContributesAndroidInjector
    internal abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ContributesAndroidInjector(modules = [CameraActivityModule::class])
    internal abstract fun contributeCameraActivity(): CameraActivity

    @ContributesAndroidInjector
    internal abstract fun contributeTakePictureActivity(): TakePictureActivity

    @ContributesAndroidInjector
    internal abstract fun contributeAllPictureActivity(): AllPictureActivity

    @ContributesAndroidInjector(modules = [AddClientActivitySubModule::class])
    internal abstract fun contributeAddClientActivity(): AddClientActivity

    @ContributesAndroidInjector(modules = [FileClaimActivitySubModule::class])
    internal abstract fun contributeFileClaim(): VerifyAnimalActivity

    @ContributesAndroidInjector(modules = [AddAnimalActivityModule::class])
    internal abstract fun contributeAddAnimalActivity(): AddAnimalActivity

    @ContributesAndroidInjector(modules = [ContactActivityModule::class])
    internal abstract fun contributeContactActivity(): ContactActivity

    @ContributesAndroidInjector(modules = [AnimalDetailActivityModule::class])
    internal abstract fun contributeAnimalDetailActivity(): AnimalDetailActivity

    @ContributesAndroidInjector(modules = [OtpActivityModule::class])
    internal abstract fun contributeOtpActivity(): OtpActivity

    @ContributesAndroidInjector
    internal abstract fun contributePolicyDetailActivity(): PolicyDetailActivity
    @ContributesAndroidInjector
    internal abstract fun contributeLanguageActivity(): LanguageActivity
}