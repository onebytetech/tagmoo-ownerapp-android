package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.add.AddClientActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class AddClientActivitySubModule {

    @Provides
    fun provideLoader(addClientActivity: AddClientActivity): ProgressHUD {
        return ProgressHUD(addClientActivity)
    }

}