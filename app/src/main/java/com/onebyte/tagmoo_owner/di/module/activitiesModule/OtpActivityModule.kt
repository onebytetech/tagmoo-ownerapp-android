package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.login.OtpActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class OtpActivityModule {

    @Provides
    fun provideLoader(homeActivity: OtpActivity): ProgressHUD {
        return ProgressHUD(homeActivity)
    }
}