package com.onebyte.tagmoo_owner.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.onebyte.tagmoo_owner.ui.contact.ContactViewModel
import com.onebyte.tagmoo_owner.di.scope.ViewModelKey
import com.onebyte.tagmoo_owner.ui.add.AddViewModel
import com.onebyte.tagmoo_owner.ui.cameraView.CameraViewModel
import com.onebyte.tagmoo_owner.ui.forgotPassword.ForgotPasswordViewModel
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.ui.login.LoginViewModel
import com.onebyte.tagmoo_owner.ui.takePictureView.TakePictureViewModel
import com.onebyte.tagmoo_owner.viewModelFactory.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    internal abstract fun bindHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ForgotPasswordViewModel::class)
    internal abstract fun bindForgotPasswordViewModel(forgotPasswordViewModel: ForgotPasswordViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CameraViewModel::class)
    internal abstract fun bindCameraViewModel(cameraViewModel: CameraViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TakePictureViewModel::class)
    internal abstract fun bindTakePictureViewModel(takePictureViewModel: TakePictureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddViewModel::class)
    internal abstract fun bindAddViewModel(addViewModel: AddViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactViewModel::class)
    internal abstract fun bindContactViewModel(contactViewModel: ContactViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}