package com.onebyte.tagmoo_owner.di.module

import com.onebyte.tagmoo_owner.utils.CustomPopups
import dagger.Module
import dagger.Provides

@Module
class CustomPopupModule {

    @Provides
    fun provideCustomPopups(): CustomPopups {
        return CustomPopups()
    }
}