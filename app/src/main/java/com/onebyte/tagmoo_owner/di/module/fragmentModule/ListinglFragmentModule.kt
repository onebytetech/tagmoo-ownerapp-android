package com.onebyte.tagmoo_owner.di.module.fragmentModule

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebyte.tagmoo_owner.ui.home.fragments.OwnerListAdapter
import com.onebyte.tagmoo_owner.ui.home.fragments.AnimalImagesAdapter
import dagger.Module
import dagger.Provides

@Module
class ListinglFragmentModule {

    @Provides
    fun provideAddedAnimalAdapter(animalImagesAdapter: AnimalImagesAdapter): OwnerListAdapter {
        return OwnerListAdapter(animalImagesAdapter)
    }

    @Provides
    fun provideAnimalImagesAdapter(): AnimalImagesAdapter {
        return AnimalImagesAdapter()
    }

    @Provides
    fun provideAddedAnimalLayoutManager(context: Context): LinearLayoutManager{
        return LinearLayoutManager(context)
    }
}