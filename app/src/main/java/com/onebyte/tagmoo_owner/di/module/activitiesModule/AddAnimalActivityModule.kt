package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.add.fragments.AddAnimalFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.AddPolicyFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CameraFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CapturedImagesFragment
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class AddAnimalActivityModule {

    @Provides
    fun provideLoader(homeActivity: HomeActivity): ProgressHUD {
        return ProgressHUD(homeActivity)
    }

    @Provides
    fun provideCameraFragment(): CameraFragment {
        return CameraFragment()
    }

    @Provides
    fun provideAddAnimalFragment(): AddAnimalFragment {
        return AddAnimalFragment()
    }

    @Provides
    fun provideCapturedImagesFragment(): CapturedImagesFragment {
        return CapturedImagesFragment()
    }

    @Provides
    fun provideAddPolicyFragment(): AddPolicyFragment {
        return AddPolicyFragment()
    }
}