package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.home.AnimalDetailActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.fragments.*
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class AnimalDetailActivityModule {

    @Provides
    fun provideLoader(homeActivity: AnimalDetailActivity): ProgressHUD {
        return ProgressHUD(homeActivity)
    }
}