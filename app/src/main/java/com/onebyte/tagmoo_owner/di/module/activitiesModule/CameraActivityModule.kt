package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.cameraView.CameraActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class CameraActivityModule {

    @Provides
    fun provideLoader(cameraActivity: CameraActivity): ProgressHUD {
        return ProgressHUD(cameraActivity)
    }
}