package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.login.OwnerLoginActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class OwnerLoginActivitySubModule {

    @Provides
    fun provideLoader(loginActivity: OwnerLoginActivity): ProgressHUD {
        return ProgressHUD(loginActivity)
    }

}