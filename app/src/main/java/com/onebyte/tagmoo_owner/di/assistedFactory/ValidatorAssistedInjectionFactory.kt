package com.onebyte.tagmoo_owner.di.assistedFactory

import androidx.databinding.ViewDataBinding
import br.com.ilhasoft.support.validation.Validator
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject

class ValidatorAssistedInjectionFactory @AssistedInject constructor(@Assisted binding: ViewDataBinding): Validator(binding){

    @AssistedInject.Factory
    interface Factory{
        fun create(binding: ViewDataBinding): ValidatorAssistedInjectionFactory
    }
}