package com.onebyte.tagmoo_owner.di.module

import android.content.SharedPreferences
import com.onebyte.tagmoo_owner.services.networkServices.NetworkService
import com.onebyte.tagmoo_owner.services.repositories.Repository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoriesModule {

    @Provides
    @Singleton
    fun provideRepository(networkService : NetworkService, sp: SharedPreferences): Repository {
        return Repository(networkService, sp)
    }
}