package com.onebyte.tagmoo_owner.di.module.activitiesModule

import com.onebyte.tagmoo_owner.ui.login.LoginActivity
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import dagger.Module
import dagger.Provides

@Module
class LoginActivitySubModule {

    @Provides
    fun provideLoader(loginActivity: LoginActivity): ProgressHUD {
        return ProgressHUD(loginActivity)
    }

}