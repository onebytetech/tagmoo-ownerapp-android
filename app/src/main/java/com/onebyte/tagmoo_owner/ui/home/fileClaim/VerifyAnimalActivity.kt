package com.onebyte.tagmoo_owner.ui.home.fileClaim

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityVerifyAnimalBinding
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ProofDocs
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import kotlinx.android.synthetic.main.activity_verify_animal.*
import javax.inject.Inject

@Suppress("SENSELESS_COMPARISON")
class VerifyAnimalActivity: BaseActivity<ActivityVerifyAnimalBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_verify_animal

    @Inject
    lateinit var progressHUD: ProgressHUD
    val TAG = VerifyAnimalActivity::class.java.simpleName
    var imageUrl = ""
    var ownerId = ""
    var animalId = ""
    lateinit var animal: Animal
    var deathCertificatePath = "0"
    var deadAnimalPath = "0"
    var cutHeadPath = "0"
    var listProofDocs = arrayListOf<ProofDocs>()
    var count = 0
    var action = ""

    lateinit var verifyAnimalFragment: VerifyAnimalFragment
    lateinit var fileClaimCameraFragment: FileClaimCameraFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentDate()
        initViews()
    }

    private fun getIntentDate() {
        if(intent.hasExtra(Constants.NavigationKeys.ownerId))
            ownerId = intent.getStringExtra(Constants.NavigationKeys.ownerId)!!
    }

    private fun initViews() {
        binding.step = 1
        verifyAnimalFragment = VerifyAnimalFragment()
        fileClaimCameraFragment = FileClaimCameraFragment()
        loadFragment(verifyAnimalFragment, verifyAnimalFragment.TAG)
        removePref()

        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    fun removePref(){
        PrefManager.removePrefrence(sp, Constants.CameraKeys.DEAD_ANIMAL)
        PrefManager.removePrefrence(sp, Constants.CameraKeys.DEATH_CERTIFICATE)
        PrefManager.removePrefrence(sp, Constants.CameraKeys.CUT_HEAD)
        PrefManager.removePrefrence(sp, Constants.CameraKeys.PROFILE_PIC)
    }

    fun loadFragment(fragment: Fragment, tag: String){
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
            .replace(R.id.flFileClaim, fragment, tag)
            .commit()
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount == 1){
            finish()
            removePref()
        }
        super.onBackPressed()
    }
}
