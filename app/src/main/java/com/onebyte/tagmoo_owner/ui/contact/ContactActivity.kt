package com.onebyte.tagmoo_owner.ui.contact

import android.os.Bundle
import androidx.lifecycle.observe
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.services.dataModels.contact.Contact
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ActivityContactBinding
import com.onebyte.tagmoo_owner.services.dataModels.contact.ContactModel
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.utils.CustomPopups
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import kotlinx.android.synthetic.main.activity_contact.*
import java.lang.Exception
import javax.inject.Inject

class ContactActivity : BaseActivity<ActivityContactBinding, ContactViewModel>() {

    override fun getViewModelClass(): Class<ContactViewModel> = ContactViewModel::class.java

    @Inject
    lateinit var progressHUD: ProgressHUD

    override fun layoutId(): Int = R.layout.activity_contact

    private lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setListeners()
        setObservable()
    }

    private fun setObservable() {

        viewModel.contactResponse.observe(this) {
            val msg: String
            msg = try {
                val res = it as ContactModel
                res.body
            } catch (ex: Exception){
                ex.toString()
            }
            customPopups.showAlertPopup(getString(R.string.str_alert), msg, this, onClick)
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }
    }

    val onClick = object: CustomPopups.OnClick{
        override fun onBackPress() {
            finish()
        }
    }

    private fun setListeners() {
        validator = component.validatorAssistedInjectionFactory.create(binding)
        tvSubmit.setOnClickListener {
            if(validator.validate()){
                val user = PrefManager.getUser(sp)
                val contact = Contact(user!!.phoneNumber, user.email, user.cnic, etMessage.text.toString(), etSubject.text.toString())
                viewModel.contact(contact)
            }
        }
    }

}
