package com.onebyte.tagmoo_owner.ui.add.fragments

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.observe
import androidx.recyclerview.widget.GridLayoutManager
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentCapturedImagesBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages
import com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels.UploadPictureResponse
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddViewModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.fragments.AnimalImagesAdapter
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.Validation
import kotlinx.android.synthetic.main.fragment_add_animal.tvNext
import kotlinx.android.synthetic.main.fragment_captured_images.*
import java.lang.Exception

class CapturedImagesFragment : BaseFragment<FragmentCapturedImagesBinding, AddViewModel>() {

    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override val layoutId: Int get()= R.layout.fragment_captured_images
    private lateinit var addedAnimal: AddedAnimal

    val TAG = CapturedImagesFragment::class.java.simpleName

    lateinit var adapterUploaded: AnimalImagesAdapter
    lateinit var adapterCaptured: CameraImagesAdapter
    lateinit var list: List<AnimalImages>
    private var count = 0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        binding.isUploading = false

        tvAddMore.setOnClickListener {
            if(!binding.isUploading!!) {
                val parent = (context as AddAnimalActivity)
                parent.binding.step = 2
                parent.onFragmentDetached(TAG)
                parent.isAddMore = true
                parent.loadFragment(parent.cameraFragment, parent.cameraFragment.TAG)
            } else
                showUploadingMessage()
        }

        tvNext.setOnClickListener {
            if(!binding.isUploading!!) {
                val parent = (context as AddAnimalActivity)
                parent.binding.step = 4
                parent.loadFragment(parent.addPolicyFragment, parent.addPolicyFragment.TAG)
            } else
                showUploadingMessage()
        }

        tvUpload.setOnClickListener {
            onClickUpload()
        }

        rvUploadedImages.layoutManager = GridLayoutManager(context, 4)
        adapterUploaded = AnimalImagesAdapter()
        rvUploadedImages.adapter = adapterUploaded

        rvImages.layoutManager = GridLayoutManager(context, 4)
        adapterCaptured = CameraImagesAdapter()
        rvImages.adapter = adapterCaptured

        binding.count = 10
        getCapturedList()
        addedAnimal = AddedAnimal(0, "", false, false, 0, list)
        viewModelObservable()
    }


     fun onClickUpload() {
        if(!binding.isUploading!!) {
            count = 0
            if (!addedAnimal.isUploading) {
                if (!list.isNullOrEmpty() && list.size > 9) {
                    uploadImage()
                } else {
                    (context as AddAnimalActivity).customPopups.showAlertPopup(
                        getString(R.string.str_alert), getString(
                            R.string.str_please_select_10_images
                        ), context as Activity, null
                    )
                }
            } else {
                showUploadingMessage()
            }
        } else {
            showUploadingMessage()
        }
    }

    private fun uploadImage(){
        try {
            if (Validation.isConnected(context!!)) {
                if (count != list.size) {
                    if (list[count].isUploaded) {
                        count++
                        uploadImage()
                    } else {
                        binding.isUploading = true
                        Constants.animal =
                            if (!addedAnimal.animal.isNullOrEmpty()) addedAnimal.animal else ""
                        addedAnimal.isUploading = true
                        viewModel!!.uploadImage(
                            addedAnimal.animalList[count].url, "5e8dd1233d34d9244766ac8d", false,
                            "Bearer " + PrefManager.loadPreferences(
                                (context as AddAnimalActivity).sp,
                                Constants.PreferenceKeys.TOKEN
                            )!!
                        )
                    }
                } else {
                    val count = getSuccessItem(addedAnimal)
                    addedAnimal.isUploaded = count > 9
                    addedAnimal.isUploading = false
                    binding.isUploading = false

                    binding.count = 10 - count
                    if (count < 10)
                        (context as AddAnimalActivity).customPopups.showAlertPopup(
                            getString(R.string.str_alert),
                            getString(R.string.str_failed_animal_message),
                            context as AddAnimalActivity,
                            null
                        )
                }
            }
        } catch (ex: Exception){
            Log.e("ERROR_UPLOAD_IMAGE", "$ex")
        }
    }

    private fun viewModelObservable() {
        viewModel!!.uploadPictureResponse().observe(this) {
            try {
                val res = it as UploadPictureResponse
                if (res.status.code == 200 && res.body != null && res.body.animal != null) {
                    addedAnimal.animal = res.body.animal
                    addedAnimal.animalList[count].isSuccess = true
                    Constants.animal = addedAnimal.animal
                } else {
                    addedAnimal.animalList[count].isSuccess = false
                }
                addedAnimal.countUploaded++
                addedAnimal.animalList[count].isUploaded = true
                count++
                Log.e("IMAGE:", "UPLOADED==>"+count)
                PrefManager.updateAnimalListPreferences(AddedAnimal(System.currentTimeMillis(),"",
                    isUploaded = false,
                    isUploading = false,
                    countUploaded = 0,
                    animalList = list
                ), (context as AddAnimalActivity).sp, 0)
                updateAdapter()
                // remove from lib and add to uploaded
                uploadImage()
            } catch (ex: Exception){
                addedAnimal.isUploading = false
                binding.isUploading = false
                Log.e("UPLOAD_IMAGE_ERROR", "$ex")
                val res = it as String
                (context as AddAnimalActivity).customPopups.showAlertPopup(getString(R.string.str_alert), res,
                    context as AddAnimalActivity, null)
            }
        }
    }

    private fun updateAdapter() {
        adapterUploaded.updateList(list, count)
        adapterCaptured.updateList(list, count)
    }

    fun getSuccessItem(addedAnimal: AddedAnimal): Int {
        var count = 0
        for (animalImage: AnimalImages in addedAnimal.animalList) {
            if (animalImage.isSuccess)
                count++
        }
        return count
    }

    private fun getCapturedList() {
        val listCaptured = PrefManager.loadNewAnimalListPreferences((context as AddAnimalActivity).sp)
        if(!listCaptured.isNullOrEmpty()) {
            list = listCaptured[listCaptured.size - 1].animalList
            adapterCaptured.updateList(list, 0)
            adapterUploaded.updateList(list, 0)
        }
    }

    fun showUploadingMessage() {
        (context as AddAnimalActivity).customPopups.showAlertPopup(
            getString(R.string.str_alert), "Photos are uploading please wait until all the photos uploaded successfully!", context as Activity, null
        )
    }
}