package com.onebyte.tagmoo_owner.ui.detail

import android.os.Bundle
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityPolicyDetailBinding
import com.onebyte.tagmoo_owner.services.dataModels.animal.RegisterPolicy
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.utils.Constants
import kotlinx.android.synthetic.main.activity_policy_detail.*

class PolicyDetailActivity : BaseActivity<ActivityPolicyDetailBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_policy_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getIntentData()
        setListeners()
    }

    private fun setListeners() {
        ivBack.setOnClickListener {
            finish()
        }
    }

    private fun getIntentData() {
        binding.policy = Gson().fromJson(intent.getStringExtra(Constants.PreferenceKeys.POLICY), RegisterPolicy::class.java)
    }
}
