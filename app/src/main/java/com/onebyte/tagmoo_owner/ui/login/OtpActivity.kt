package com.onebyte.tagmoo_owner.ui.login

import android.os.Bundle
import android.util.Log
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ActivityOtpBinding
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import java.lang.Exception
import javax.inject.Inject
import androidx.lifecycle.observe
import com.onebyte.tagmoo_owner.ui.home.OwnerHomeActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_otp.*

class OtpActivity : BaseActivity<ActivityOtpBinding, LoginViewModel>() {
    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_otp

    @Inject
    lateinit var progressHUD: ProgressHUD

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validatorInitializer()
        viewModelObservables()
        setListeners()
    }

    private fun validatorInitializer() {
        validator = component.validatorAssistedInjectionFactory.create(binding)
    }

    private fun viewModelObservables(){
        viewModel.otpResponse().observe(this){
            try {
                it.let {
                    val res = it as UserModel
                    if (res.status.code == 200) {

                        Log.e("Data", res.body.user.firstName)
                        PrefManager.savePreferences(Constants.PreferenceKeys.TOKEN, "Bearer " + res.body.user.token, sp)
                        PrefManager.saveUser(res.body.user, sp)
                        navigateActivity(OwnerHomeActivity::class.java, this@OtpActivity, true)
                    }else{
                        customPopups.showAlertPopup("", res.status.message, this, null)

                    }
                }
            } catch (ex: Exception) {
                it.let {
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }


    private fun setListeners() {
        tvNextSend.setOnClickListener{
            if (Validation.isConnected(this) && validator.validate()){
                viewModel.verifyOtp(etPTO.text.toString(), intent.getStringExtra(Constants.PreferenceKeys.TOKEN)!!)
            }
        }

    }
}
