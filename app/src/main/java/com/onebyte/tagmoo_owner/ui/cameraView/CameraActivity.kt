package com.onebyte.tagmoo_owner.ui.cameraView

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.onebyte.tagmoo_owner.databinding.ActivityCameraBinding
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_camera.*
import java.io.File
import java.util.concurrent.Executors
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Environment
import androidx.core.content.FileProvider
import com.onebyte.tagmoo_owner.BuildConfig
import java.lang.Exception
import android.provider.MediaStore
import android.provider.Settings
import android.view.View
import android.widget.ArrayAdapter
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ProofDocs
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.coroutines.*
import java.io.FileOutputStream
import java.io.IOException

@Suppress("PrivatePropertyName")
class CameraActivity : BaseActivity<ActivityCameraBinding, CameraViewModel>() {
    override fun getViewModelClass(): Class<CameraViewModel> = CameraViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_camera
    private val CHOOSE_GALLERY_IMAGE = 2
    private val PIC_CROP = 1
    private val REQUEST_CODE_PERMISSIONS = 10
    private val READ_WRITE_PER_CODE = 11
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    var mCropImagedUri : Uri? = null

    var actionKey = ""

    var path = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setListeners()
        initView()
        getIntentData()
    }

    private fun getIntentData() {
        binding.isSupportDocs = intent.hasExtra(Constants.CameraKeys.SUPPORTING_DOCS)
        if(binding.isSupportDocs!!){
            var list = arrayListOf<String>()
            list.add("Cut off head")
            list.add("Dead animal with owner")
            list.add("Animal's death certificate")
            val adapter = ArrayAdapter<String>(this, R.layout.layout_spinner, list)
            spTitle.setAdapter(adapter)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spTitle.setAdapter(adapter)
            spTitle.setEditable(false)
        } else{
            actionKey = if(intent.hasExtra(Constants.CameraKeys.PROFILE_PIC))
                Constants.CameraKeys.PROFILE_PIC
            else
                Constants.PreferenceKeys.compare
        }
    }

    private fun initView() {
        if (allPermissionsGranted()) {
            cameraView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
    }

    private val executor = Executors.newSingleThreadExecutor()

    private fun setListeners() {
        spTitle.setOnItemClickListener { _, _, position, _ ->
            when (position) {
                0 -> actionKey = Constants.CameraKeys.CUT_HEAD
                1 -> actionKey = Constants.CameraKeys.DEAD_ANIMAL
                2 -> actionKey = Constants.CameraKeys.DEATH_CERTIFICATE
            }
        }
        tvRetake.setOnClickListener {
            ivShow.visibility = View.GONE
            tvRetake.visibility = View.GONE
        }
        tvFinish.setOnClickListener {
            if(tvRetake.visibility == View.VISIBLE) {
                if(actionKey.equals(Constants.PreferenceKeys.compare))
                    PrefManager.savePreferences(Constants.CameraKeys.PROFILE_PIC, path, sp)
                else
                    PrefManager.savePreferences(actionKey, path, sp)
                finish()
            } else
                customPopups.showAlertPopup(getString(R.string.str_alert), "Please take a picture", this, null)
        }

        cameraView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }

        tvAdd.setOnClickListener {
            var msg = ""
            if(tvRetake.visibility == View.VISIBLE){
                if(spTitle.text.toString().isNotEmpty() && etDetail.text.toString().isNotEmpty()) {
                    val proof = ProofDocs(spTitle.text.toString(), path, etDetail.text.toString())
                    PrefManager.savePreferences(actionKey, Gson().toJson(proof), sp)
                    finish()
                } else {
                    msg = "Please select title and enter detail"
                }
            } else
                msg = "Please take a picture"

            customPopups.showAlertPopup(getString(R.string.str_alert), msg, this, null)
        }
    }

    private fun requestExternalPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissions = arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            requestPermissions(permissions, READ_WRITE_PER_CODE)
        }
    }

    private fun checkExternalPermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                customPopups.showAlertPopup(getString(R.string.str_alert),getString(R.string.str_gallery_images_detail), this, onGalleryClick)
                true
            } else {
                false
            }
        } else
            true
    }

    private val onGalleryClick = object: CustomPopups.OnClick{
        override fun onBackPress() {
            val picImageIntent = getImageIntent(true, null)
            startActivityForResult(picImageIntent, CHOOSE_GALLERY_IMAGE)
        }
    }

    private fun getImageIntent(isGallery: Boolean, picUri: Uri?): Intent{
        val cropIntent: Intent
        if(isGallery) {
            cropIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            cropIntent.type = "image/*"
        } else {
            cropIntent = Intent("com.android.camera.action.CROP")
            cropIntent.setDataAndType(picUri, "image/*")
            cropIntent.putExtra("crop", true)

            val f = createNewFile()
            try {
                f.createNewFile()
            } catch (ex: IOException) {
                Log.e("IO_EXCEPTION", "$ex")
            }

            mCropImagedUri = Uri.fromFile(f)
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri)
        }
        cropIntent.putExtra("aspectX", 3)
        cropIntent.putExtra("aspectY", 2)
        cropIntent.putExtra("scale", true)
        cropIntent.putExtra("outputX", 2048)
        cropIntent.putExtra("outputY", 1365)
        cropIntent.putExtra("return-data", false)
        cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

        return cropIntent
    }

    private fun performCrop(picUri: Uri) {
        try {
            val cropIntent = getImageIntent(false, picUri)
            startActivityForResult(cropIntent, PIC_CROP)
        } catch (aNFE: ActivityNotFoundException) {
            Toast.makeText(this, getString(R.string.str_crop_device_message), Toast.LENGTH_SHORT).show()

            PrefManager.savePreferences(Constants.PreferenceKeys.imageUrl, picUri.path.toString() , sp)
            finish()
        }
    }

    private fun createNewFile() :File {
        val newDirectory : File = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).absolutePath)
        }else {
            File((this).getExternalFilesDir(null)!!.absolutePath)
        }
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d("DIRECTORY_PATH", newDirectory.absolutePath)
            }
        }
        val file = File(newDirectory, ("CROP_" + System.currentTimeMillis() + ".jpg"))
        if (file.exists()) {

            file.delete()
            try {
                file.createNewFile()
            } catch (e: IOException) {
                Log.e("NEW_FILE_CREATE_ERROR","$e")
            }
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
//            if (data != null && data.data != null)
            if(resultCode == Activity.RESULT_OK && requestCode == 1) {
                if (data != null) {
                    var imagePath = data.data.toString()
                    if (data.data == null) {
                        imagePath = mCropImagedUri.toString()
                    }

                    when (requestCode) {
                        CHOOSE_GALLERY_IMAGE -> {
                            val proj = arrayOf("_data")
                            val cursor = contentResolver.query(data.data!!, proj, null, null, null)
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    val columnIndex = cursor.getColumnIndexOrThrow(proj[0])
                                    imagePath = cursor.getString(columnIndex)
                                }
                                cursor.close()
                            }
                        }
                    }
                    ivShow.visibility = View.VISIBLE
                    ivShow.loadImage(imagePath)
                    tvRetake.visibility = View.VISIBLE
//                PrefManager.savePreferences(Constants.PreferenceKeys.imageUrl, imagePath
//                    .replace("file:",""), sp)
//                finish()
                }
            }
        } catch (ex: Exception){
            Log.e("CAMERA_IMAGE_ERROR", "$ex")
        }
    }

    private fun startCamera() {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(cameraView.width, cameraView.height))
        }.build()

        val preview = Preview(previewConfig)
        preview.setOnPreviewOutputUpdateListener {

            val parent = cameraView.parent as ViewGroup
            parent.removeView(cameraView)
            parent.addView(cameraView, 0)

            cameraView.surfaceTexture = it.surfaceTexture
            updateTransform()
        }


        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            }.build()

        val imageCapture = ImageCapture(imageCaptureConfig)

        ivCapture.setOnClickListener {
            val file = File(externalMediaDirs.first(),
                "${System.currentTimeMillis()}.jpg")

            imageCapture.takePicture(file, executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        exc: Throwable?) {

                        val msg = "Photo capture failed: $message"
                        Log.e("CameraXApp", msg, exc)
                        cameraView.post { Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show() }
                    }

                    override fun onImageSaved(file: File) {
                        cameraView.post {
                            ivShow.visibility = View.VISIBLE
                            ivShow.loadImage(file.absolutePath)
                            tvRetake.visibility = View.VISIBLE
                            path = file.absolutePath
                        }
                    }
                })
        }

        val analyzerConfig = ImageAnalysisConfig.Builder().apply {
            setImageReaderMode(
                ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        }.build()

        val analyzerUseCase = ImageAnalysis(analyzerConfig).apply {
            setAnalyzer(executor, LuminosityAnalyzer())
        }

        CameraX.bindToLifecycle(this, preview, imageCapture, analyzerUseCase)
    }

    fun cropImage(imageUri: Uri){
        try {
            MainScope().launch {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                val matrix = Matrix()
                matrix.postRotate(90f)

                val width = bitmap.width
                val height = bitmap.height
                var newWidth = if ((height > width)) width else height
                var newHeight = if ((height > width)) height - (height - width) else height
                newWidth -= 530
                newHeight -= 200
                val cropImg = Bitmap.createBitmap(bitmap, 400, 0, newWidth, newHeight)

                val rotatedBitmap =
                    Bitmap.createBitmap(cropImg, 0, 0, cropImg.width, cropImg.height, matrix, true)

                try {
                    FileOutputStream(File(imageUri.path!!)).use { out ->
                        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                withContext(Dispatchers.Main){
                    //                    ivShow.visibility = View.VISIBLE
//                    binding.path = imageUri.path
                }
            }

        } catch (ex: Exception){
            Log.e("IMAGE_CROP_ERROR", "$ex")
            customPopups.showAlertPopup(getString(R.string.str_alert), getString(R.string.str_auto_crop_message), this, null)
        }
    }

    private fun updateTransform() {
        val matrix = Matrix()

        val centerX = cameraView.width / 2f
        val centerY = cameraView.height / 2f

        val rotationDegrees = when(cameraView.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        cameraView.setTransform(matrix)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> if (allPermissionsGranted()) {
                cameraView.post { startCamera() }
            } else {
                customPopups.showConfirmPopup(getString(R.string.str_camera_permission_title),
                        getString(R.string.str_camera_permission_detail), this, onClick)
            }
            READ_WRITE_PER_CODE -> {
                if(!checkExternalPermissionGranted()){
                    customPopups.showConfirmPopup(getString(R.string.str_external_permission_title),
                        getString(R.string.str_external_permission_detail), this, onClickExternalPermission)
                }
            }
        }
    }

    private val onClick = object: CustomPopups.OnClickConfirm{
        override fun onBackPress(isYes: Boolean) {
            if(isYes) {
                var showRationale = true
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)
                if (!showRationale) {
                    goToSetting()
                } else
                    initView()
            } else
                finish()
        }
    }

    fun goToSetting() {
        val i = Intent()
        i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        i.addCategory(Intent.CATEGORY_DEFAULT)
        i.data = Uri.parse("package:$packageName")
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        startActivity(i)
        finish()
    }

    private val onClickExternalPermission = object: CustomPopups.OnClickConfirm{
        override fun onBackPress(isYes: Boolean) {
            if(isYes)
                requestExternalPermission()
            else
                finish()
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            baseContext, it) == PackageManager.PERMISSION_GRANTED
    }
}
