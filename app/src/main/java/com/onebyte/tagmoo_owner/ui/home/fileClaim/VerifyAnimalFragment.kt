package com.onebyte.tagmoo_owner.ui.home.fileClaim

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentVerifyAnimalBinding
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ProofDocs
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalCompare
import com.onebyte.tagmoo_owner.services.dataModels.errorModel.StatusModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import androidx.lifecycle.observe
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ClaimModel
import com.onebyte.tagmoo_owner.services.dataModels.uploadImageModels.UploadImageModel
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.fragment_verify_animal.*

@Suppress("SENSELESS_COMPARISON")
class VerifyAnimalFragment: BaseFragment<FragmentVerifyAnimalBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_verify_animal

    val TAG = VerifyAnimalFragment::class.java.simpleName
    lateinit var animal: Animal
    var listProofDocs = arrayListOf<ProofDocs>()
    var count = 0
    lateinit var parent : VerifyAnimalActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModelObservable()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
    }

    private fun viewModelObservable() {
        viewModel!!.uploadPictureResponse().observe(this) {
            hideLoader(parent.progressHUD)
            try {
                val res = it as AnimalCompare
                if (res.body != null && res.body != null) {
                        val msg: String
                        val title: String
                        if(res.body.animalId.isNullOrEmpty()){
                            title = "Failed"
                            msg = res.status.message
                            parent.customPopups.showAlertPopup(title, msg, parent, null)
                        } else {
                            binding.animal = res.body.animal
                            animal = res.body.animal
                            animal.name = res.body.animalId
                            title = "Animal Verified"
                            msg = "Your Animal's ID is:" +
                                    " ${res.body.animalId}" + "\n\n\nIs this the animal you are trying to claim?? \nIf isn't tap No button and retake image."
                            parent.customPopups.showConfirmPopup(title, msg, parent, onClickPopup)
                        }
                } else if (res.status != null && res.status.message != null) {
                    parent.customPopups.showAlertPopup(getString(R.string.str_alert), res.status.message, parent, null)
                    Log.e("UPLOADED_IMAGE_ERROR", res.status.message)
                }
            } catch (ex: Exception) {
                try{
                    parent.customPopups.showAlertPopup(getString(R.string.str_alert), Gson().fromJson(it as String, StatusModel::class.java).status!!.message, parent, null)
                } catch (ex: Exception) {
                    parent.customPopups.showAlertPopup(getString(R.string.str_alert), it as String, parent, null)
                }
            }
        }

        viewModel!!.claimApiResponse().observe(this){
            hideLoader(parent.progressHUD)
            try {
                count = 0
                it.let {
                    val res = it as ClaimModel
                    if (res.status.code == 200) {

                        Log.e("Data", res.body._id)
                        parent.toast("File Claim  Successfully!", Toast.LENGTH_LONG)
                        parent.finish()
                    }else{
                        parent.customPopups.showAlertPopup("", res.status.message, parent, null)
                    }
                }
            } catch (ex: Exception) {
                it.let {
                    parent.customPopups.showAlertPopup("", ex.message.toString(), parent, null)
                }
            }
        }

        viewModel!!.uploadImageResponse().observe(this){
            try {
                it.let {
                    val res = it as UploadImageModel
                    listProofDocs[count].docUrl = res.body!!.image
                    count ++
                    if(count == listProofDocs.size) {
                        viewModel!!.claim.value!!.ownerId = parent.ownerId
                        viewModel!!.claim.value!!.animalId = animal._id
                        viewModel!!.claim.value!!.registeredPolicyId = animal.registeredPolicyId
                        viewModel!!.claim.value!!.reason = "Animal Is Dead"
                        viewModel!!.claim.value!!.mlResult = animal.name
                        viewModel!!.claim.value!!.proofDocs = listProofDocs
                        count = 0
                        viewModel!!.fileClaim()
                    } else{
                        viewModel!!.uploadImage(listProofDocs[count].docUrl)
                    }
                }
            } catch (ex: Exception) {
                hideLoader(parent.progressHUD)
                count = 0
                try{
                    parent.customPopups.showAlertPopup(getString(R.string.str_alert), Gson().fromJson(it.toString(), StatusModel::class.java).status!!.message, parent, null)

                } catch (e: java.lang.Exception) {
                    parent.customPopups.showAlertPopup(getString(R.string.str_alert), e.message.toString(), parent, null)
                }
            }
        }
    }

    private val onClickPopup = object: CustomPopups.OnClickConfirm{
        override fun onBackPress(isYes: Boolean) {
            if (isYes) {
                binding.isVerified = true
                parent.binding.step = 3
            } else {
                binding.isVerified = false
                binding.isShowImage = false
                PrefManager.removePrefrence(sp, Constants.CameraKeys.PROFILE_PIC)

                parent.action = Constants.PreferenceKeys.compare
                parent.loadFragment(parent.fileClaimCameraFragment, parent.fileClaimCameraFragment.TAG)
            }
        }
    }

    private fun initViews() {
        parent = context as VerifyAnimalActivity
        binding.isShowImage = false
        binding.isVerified = false
        onResumeFun()
    }

    private fun setListeners() {
        tvSubmitDocs.setOnClickListener {
            parent.binding.step = 3
            parent.action = Constants.CameraKeys.SUPPORTING_DOCS
            parent.loadFragment(parent.fileClaimCameraFragment, parent.fileClaimCameraFragment.TAG)
        }

        tvRetake.setOnClickListener {
            parent.binding.step = 2
            parent.action = Constants.PreferenceKeys.compare
            parent.loadFragment(parent.fileClaimCameraFragment, parent.fileClaimCameraFragment.TAG)
        }

        tvSubmit.setOnClickListener {
            if (Validation.isConnected(parent)){
                showLoader(parent.progressHUD)
                viewModel!!.uploadImage(parent.imageUrl, parent.ownerId,true,
                    "Bearer "+PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
            }
        }

        tvFileClaim.setOnClickListener {
            if (Validation.isConnected(parent)){
                if (animal.registeredPolicyId.isNullOrEmpty()){
                    parent.customPopups.showAlertPopup("", "Detailed Information is not available.", parent, null)
                }else{
                    uploadImagesProofDocs()
                }

            }
        }
    }

    private fun uploadImagesProofDocs() {
        if (!parent.cutHeadPath.equals("0")) {
            if (!parent.deadAnimalPath.equals("0")) {
                showLoader(parent.progressHUD)
                viewModel!!.uploadImage(listProofDocs[count].docUrl)
            } else
                parent.customPopups.showAlertPopup(getString(R.string.str_alert), "Please take a picture of Client with dead animal", parent, null)
        } else
            parent.customPopups.showAlertPopup(getString(R.string.str_alert), "Please take a picture of cut off head", parent, null)
    }

    fun onResumeFun() {
        binding.step = parent.binding.step

        parent.imageUrl = PrefManager.loadPreferences(sp, Constants.CameraKeys.PROFILE_PIC)!!
        if (parent.imageUrl != "0") {
            binding.isShowImage = parent.action.equals(Constants.PreferenceKeys.compare)
            ImageLoadAdapter.loadImage(ivShow, parent.imageUrl)
        }

        if(parent.action.equals(Constants.CameraKeys.SUPPORTING_DOCS)) {
            binding.isVerified = true
            binding.animal = animal
        }

        parent.deadAnimalPath = PrefManager.loadPreferences(sp, Constants.CameraKeys.DEAD_ANIMAL)!!
        parent.cutHeadPath = PrefManager.loadPreferences(sp, Constants.CameraKeys.CUT_HEAD)!!
        parent.deathCertificatePath = PrefManager.loadPreferences(sp, Constants.CameraKeys.DEATH_CERTIFICATE)!!
        var gson = Gson()
        if (parent.deadAnimalPath != "0") {
            val proofDocs = gson.fromJson<ProofDocs>(parent.deadAnimalPath, ProofDocs::class.java)
            val obj = listProofDocs.find { it.title.equals(proofDocs.title) }
            var pos = -1
            if(obj != null)
                pos = listProofDocs.indexOf(obj)
            if(pos == -1)
                listProofDocs.add(proofDocs)
            else {
                listProofDocs.removeAt(pos)
                listProofDocs.add(proofDocs)
            }
            ImageLoadAdapter.loadImage(ivDeadAnimal, proofDocs.docUrl)
        }

        if (parent.cutHeadPath != "0") {
            val proofDocs = gson.fromJson<ProofDocs>(parent.cutHeadPath, ProofDocs::class.java)
            val obj = listProofDocs.find { it.title.equals(proofDocs.title) }
            var pos = -1
            if(obj != null)
                pos = listProofDocs.indexOf(obj)
            if(pos == -1)
                listProofDocs.add(proofDocs)
            else {
                listProofDocs.removeAt(pos)
                listProofDocs.add(proofDocs)
            }
            ImageLoadAdapter.loadImage(ivCutHead, proofDocs.docUrl)
        }

        if (parent.deathCertificatePath != "0") {
            val proofDocs = gson.fromJson<ProofDocs>(parent.deathCertificatePath, ProofDocs::class.java)
            val obj = listProofDocs.find { it.title.equals(proofDocs.title) }
            var pos = -1
            if(obj != null)
                pos = listProofDocs.indexOf(obj)
            if(pos == -1)
                listProofDocs.add(proofDocs)
            else {
                listProofDocs.removeAt(pos)
                listProofDocs.add(proofDocs)
            }
            ImageLoadAdapter.loadImage(ivDeathCertificate, proofDocs.docUrl)
        }

        if(parent.deadAnimalPath != "0" && parent.cutHeadPath != "0") {
            tvFileClaim.visibility = View.VISIBLE
            parent.binding.step = 4
        }
    }
}
