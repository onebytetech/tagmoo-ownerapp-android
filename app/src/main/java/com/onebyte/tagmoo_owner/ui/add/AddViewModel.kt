package com.onebyte.tagmoo_owner.ui.add

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.Owner
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.services.repositories.Repository
import com.onebyte.tagmoo_owner.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class AddViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var repository: Repository
    val user = MutableLiveData(Owner())
    private val apiResponse: MutableLiveData<Any> = MutableLiveData()
    fun apiResponse(): LiveData<Any> = apiResponse

    val isLoading = MutableLiveData<Boolean>()

    private val uploadPictureResponse: MutableLiveData<Any> = MutableLiveData()

    fun uploadPictureResponse(): LiveData<Any> = uploadPictureResponse

    fun uploadImage(imageUrl: String, ownerId: String, isCompare: Boolean, token: String) {
        try {
            isLoading.postValue(true)
            viewModelScope.launch(Dispatchers.IO) {

                runCatching {
                    repository.uploadImage(imageUrl, ownerId, isCompare, token)

                }.onSuccess {
                    isLoading.postValue(false)

                    withContext(Dispatchers.Main) {
                        if (it.isSuccessful)
                            uploadPictureResponse.postValue(it.body())
                        else {
                            uploadPictureResponse.postValue(it.errorBody()!!.string())
                            Log.e("ERROR_IMAGE_UPLOAD", it.errorBody()!!.string())
                        }
                    }
                }.onFailure {
                    isLoading.postValue(false)
                    uploadPictureResponse.postValue(it.message)
                    Log.e("ERROR_IMAGE_UPLOAD", it.message!!)

                }
            }
        } catch (ex: Exception){
            Log.e("SOME_THING_WRONG", "$ex")
        }
    }
    fun addOwner(token: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.addOwner(user.value!!, token)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        apiResponse.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: UserModel = gson.fromJson(
                                error,
                                UserModel::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            apiResponse.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                apiResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun loadDataCoroutine() {
    }
}