package com.onebyte.tagmoo_owner.ui.add.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ItemCameraImagesBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages

class CameraImagesAdapter:
    RecyclerView.Adapter<CameraImagesAdapter.ViewHolder>() {
    override fun getItemCount(): Int {
        return list.size
    }

    var context: Context? = null
    var list: List<AnimalImages> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_camera_images, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            bind(list[position])
        }
    }

    fun updateList(newList: List<AnimalImages>, position: Int) {
        (list as ArrayList).clear()
        for (item: AnimalImages in newList){
            if(!item.isUploaded)
                (list as ArrayList).add(item)
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemCameraImagesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(animalImages: AnimalImages) {
            with(binding) {
                try {
                    tag = layoutPosition.toString()
                   animalImage = animalImages
                } catch (ex: Exception) {
                }
            }
        }
    }
}