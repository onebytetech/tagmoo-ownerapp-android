package com.onebyte.tagmoo_owner.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentHomeBinding
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddClientActivity
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.utils.navigateActivity
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_home

    val TAG = HomeFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setListeners()
    }

    private fun setListeners() {
        tvAddClient.setOnClickListener {
            navigateActivity(AddClientActivity::class.java, activity as HomeActivity, false)
        }

        tvAddAnimal.setOnClickListener {
            navigateActivity(AddAnimalActivity::class.java, activity as HomeActivity, false)
        }
    }

    private fun initViews() {
    }
}
