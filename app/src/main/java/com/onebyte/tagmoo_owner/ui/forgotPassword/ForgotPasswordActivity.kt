package com.onebyte.tagmoo_owner.ui.forgotPassword

import android.os.Bundle
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityForgotPasswordBinding
import com.onebyte.tagmoo_owner.ui.base.BaseActivity

class ForgotPasswordActivity : BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>() {
    override fun getViewModelClass(): Class<ForgotPasswordViewModel> = ForgotPasswordViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_forgot_password

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
}
