package com.onebyte.tagmoo_owner.ui.home

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityOwnerHomeBinding
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalModels
import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.contact.ContactActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.ui.home.fragments.AnimalListAdapter
import com.onebyte.tagmoo_owner.ui.login.LanguageActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_owner_home.*
import kotlinx.android.synthetic.main.activity_owner_home.rvOwner
import kotlinx.android.synthetic.main.fragment_listing.etSearch
import java.lang.Exception
import javax.inject.Inject

class OwnerHomeActivity : BaseActivity<ActivityOwnerHomeBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    @Inject
    lateinit var progressHUD: ProgressHUD

    override fun layoutId(): Int = R.layout.activity_owner_home

    @Inject
    lateinit var animalListAdapter: AnimalListAdapter

    var isSearching = false

    var list = arrayListOf<Animal>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setListeners()
        initViews()
    }

    private fun setListeners() {
        rbClaimed.setOnCheckedChangeListener {_, isChecked ->
            if(isChecked){
                hideKeyboard()
                isSearching = false
                etSearch.text.clear()
                viewModel.ownerAnimalList(false, "claimed")
            }
        }

        rbIncomplete.setOnCheckedChangeListener {_, isChecked ->
            if(isChecked){
                hideKeyboard()
                isSearching = false
                etSearch.text.clear()
                viewModel.ownerAnimalList(false, "incomplete")
            }
        }

        rbAll.setOnCheckedChangeListener {_, isChecked ->
            if(isChecked && !isSearching){
                hideKeyboard()
                etSearch.text.clear()
                viewModel.ownerAnimalList(false)
            }
        }

        tvFileClaim.setOnClickListener {
            val user = PrefManager.getUser(sp)
            val ownerId = if(user?._id != null) user._id else ""
            navigateActivityWithExtra(
                VerifyAnimalActivity::class.java, this,
                Constants.NavigationKeys.ownerId, ownerId, false)
        }

        etSearch.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try{
                    if(s.toString().isNotEmpty()) {
                        isSearching = true
                        rbAll.isChecked = true
                        viewModel.ownerAnimalList(true, s.toString())
                    } else
                        viewModel.ownerAnimalList(true)
                } catch (ex:Exception){
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        btnMenu.setOnClickListener {
            navigateActivity(ContactActivity::class.java, this, false)
        }

        btnSettings.setOnClickListener {
            navigateActivityWithExtra(LanguageActivity::class.java, this, "from", "settings",true)

        }
    }

    private fun initViews() {
        binding.isDataFound = false
        binding.selected = 1
        rvOwner.layoutManager = LinearLayoutManager(this)
        rvOwner.adapter = animalListAdapter
        if (Validation.isConnected(this)){
            viewModel.ownerAnimalList(false)
        }
        viewModelObservables()
    }

    private fun viewModelObservables(){
        viewModel.ownerAnimalListApiResponse().observe(this){
            try {
                it.let {
                    val res = it as AnimalModels
                    if (res.status.code == 200 && !res.body!!.animalIds.isNullOrEmpty()) {
                        list.addAll(res.body.animalIds)
                        animalListAdapter.updateList(res.body.animalIds)
                        binding.isDataFound = res.body.animalIds.isNotEmpty()
                    }else{
                        binding.isDataFound = false
                    }
                }
            } catch (ex: Exception) {
                binding.isDataFound = false
                try {
                    customPopups.showAlertPopup("", Gson().fromJson(it.toString(), Status::class.java).message, this, null)
                } catch (ex: Exception){
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }


}
