package com.onebyte.tagmoo_owner.ui.home.fragments

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentAddedAnimalBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages
import com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels.UploadPictureResponse
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.Validation
import java.lang.Exception
import javax.inject.Inject

class AddedAnimalFragment : BaseFragment<FragmentAddedAnimalBinding, HomeViewModel>(), AddedAnimalAdapter.OnClick  {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_added_animal

    private lateinit var addedAnimal: AddedAnimal
    private var count = 0
    private var pos = -1
    val TAG = AddedAnimalFragment::class.java.simpleName
    var isUploading = false

    @Inject
    lateinit var addedAnimalAdapter: AddedAnimalAdapter

    @Inject
    lateinit var linearLayoutManager: LinearLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelObservable()
        initViews()
        setListeners()
    }

    private fun viewModelObservable() {
        viewModel!!.uploadPictureResponse().observe(this) {
            try {
                val res = it as UploadPictureResponse
                if (res.status.code == 200 && res.body != null && res.body.animal != null) {
                    addedAnimal.animal = res.body.animal
                    addedAnimal.animalList[count].isSuccess = true
                    Constants.animal = addedAnimal.animal
                } else {
                    addedAnimal.animalList[count].isSuccess = false
                }
                addedAnimal.countUploaded++
                addedAnimal.animalList[count].isUploaded = true
                count++
                Log.e("IMAGE:", "UPLOADED")
                updateAdapter(PrefManager.updateAnimalListPreferences(addedAnimal, (context as HomeActivity).sp, pos))
                uploadImage()
            } catch (ex: Exception){
                addedAnimal.isUploading = false
                isUploading = false
                updateAdapter(PrefManager.updateAnimalListPreferences(addedAnimal, (context as HomeActivity).sp, pos))
                Log.e("UPLOAD_IMAGE_ERROR", "$ex")
                val res = it as String
                (context as HomeActivity).customPopups.showAlertPopup(getString(R.string.str_alert), res,
                    context as HomeActivity, null)
            }
        }
    }

    private fun updateAdapter(list: List<AddedAnimal>) {
        addedAnimalAdapter.updateList(list, pos)
    }

    private fun getListAddedAnimal(): List<AddedAnimal> {
        return PrefManager.loadNewAnimalListPreferences((context as HomeActivity).sp)
    }

    private fun initViews() {
        binding.rvAddedAnimal.layoutManager = linearLayoutManager
        binding.rvAddedAnimal.adapter = addedAnimalAdapter
        addedAnimalAdapter.setOnClick(this)
    }

    override fun onClickUpload(pos: Int) {
        if(!isUploading) {
            this.pos = pos
            count = 0
            val list = getListAddedAnimal()
            addedAnimal = list[pos]
            if (!addedAnimal.isUploading) {
                if (!addedAnimal.isUploaded && !addedAnimal.animalList.isNullOrEmpty() && addedAnimal.animalList.size > 9) {
                    uploadImage()
                } else {
                    (context as HomeActivity).customPopups.showAlertPopup(
                        getString(R.string.str_alert), getString(
                            R.string.str_please_select_10_images
                        ), context as Activity, null
                    )
                }
            } else {
                showUploadingMessage()
            }
        } else {
            showUploadingMessage()
        }
    }

    fun showUploadingMessage() {
        (context as HomeActivity).customPopups.showAlertPopup(
            getString(R.string.str_alert), "Photos are uploading please wait until all the photos uploaded successfully!", context as Activity, null
        )
    }

    override fun onResume() {
        super.onResume()
        val list = getListAddedAnimal()
        binding.isDataLoaded = list.isNotEmpty()
        updateAdapter(list)
    }

    private fun uploadImage(){
        if(Validation.isConnected(context!!)) {
            if (count != addedAnimal.animalList.size) {
                if (addedAnimal.animalList[count].isUploaded) {
                    count++
                    uploadImage()
                } else {
                    isUploading = true
                    Constants.animal =
                        if (!addedAnimal.animal.isNullOrEmpty()) addedAnimal.animal else ""
                    addedAnimal.isUploading = true
                    PrefManager.updateAnimalListPreferences(
                        addedAnimal,
                        (context as HomeActivity).sp,
                        pos
                    )
                    updateAdapter(getListAddedAnimal())
//                    viewModel!!.uploadImage(addedAnimal.animalList[count].url, false, PrefManager.loadPreferences((context as HomeActivity).sp, Constants.PreferenceKeys.TOKEN)!!)

                }
            } else {
                val count = getSuccessItem(addedAnimal)
                addedAnimal.isUploaded = count > 9
                addedAnimal.isUploading = false
                isUploading = false
                updateAdapter(
                    PrefManager.updateAnimalListPreferences(
                        addedAnimal,
                        (context as HomeActivity).sp,
                        pos
                    )
                )
                if (count < 10)
                    (context as HomeActivity).customPopups.showAlertPopup(
                        getString(R.string.str_alert),
                        getString(R.string.str_failed_animal_message), context as HomeActivity, null
                    )
            }
        }
    }

    fun getSuccessItem(addedAnimal: AddedAnimal): Int {
        var count = 0
        for (animalImage: AnimalImages in addedAnimal.animalList) {
            if (animalImage.isSuccess)
                count++
        }
        return count
    }

    fun onFinish(){
        if(::addedAnimal.isInitialized){
            addedAnimal.isUploading = false
            isUploading = false
            PrefManager.updateAnimalListPreferences(addedAnimal, (context as HomeActivity).sp, pos)
        }
    }

    private fun setListeners() {
    }
}
