package com.onebyte.tagmoo_owner.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityHomeBinding
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.cameraView.CameraActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.ui.home.fragments.*
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import com.onebyte.tagmoo_owner.utils.navigateActivity
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity<ActivityHomeBinding, HomeViewModel>() {

    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    @Inject
    lateinit var progressHUD: ProgressHUD

    @Inject
    lateinit var homeFragment: HomeFragment

    @Inject
    lateinit var listingFragment: ListingFragment

    @Inject
    lateinit var financialFragment: FinancialFragment

    @Inject
    lateinit var settingsFragment: SettingsFragment

    @Inject
    lateinit var verifyAnimal: VerifyAnimalActivity

    override fun layoutId(): Int = R.layout.activity_home

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setListeners()
    }

    private val navigationItemClickListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.nav_home -> {
                    loadFragment(homeFragment, homeFragment.TAG)
                }
                R.id.nav_listing -> {
                    loadFragment(listingFragment, listingFragment.TAG)

                }
                R.id.nav_add -> {
//                    loadFragment(verifyAnimal, verifyAnimal.TAG)
                }
                R.id.nav_financial -> {
                    loadFragment(financialFragment, financialFragment.TAG)

                }
                R.id.nav_setting -> {
                    loadFragment(settingsFragment, settingsFragment.TAG)

                }
            }
            true
        }

    private fun loadFragment(fragment: Fragment, tag: String){
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
            .replace(R.id.flHomeContainer, fragment, tag)
            .commit()
    }

    private fun initViews() {
        loadFragment(homeFragment, homeFragment.TAG)
    }

    private fun setListeners() {
        navigation.setOnNavigationItemSelectedListener(navigationItemClickListener)
        btnAdd.setOnClickListener {
            navigateActivity(CameraActivity::class.java, this, false)
        }
    }

    override fun onBackPressed() {
        finish()
    }
}
