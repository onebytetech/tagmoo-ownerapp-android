package com.onebyte.tagmoo_owner.ui.home.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.*
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ItemAnimalBinding
import com.onebyte.tagmoo_owner.services.dataModels.animal.Animal
import com.onebyte.tagmoo_owner.ui.home.AnimalDetailActivity
import com.onebyte.tagmoo_owner.ui.home.OwnerHomeActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.navigateActivityWithExtra
import javax.inject.Inject

class AnimalListAdapter @Inject constructor(var animalImagesAdapter: AnimalImagesAdapter):
    RecyclerView.Adapter<AnimalListAdapter.ViewHolder>() {

    var list: List<Animal> = arrayListOf()

    override fun getItemCount(): Int {
        return list.size
    }

    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_animal, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
//            bind(createFileClaimListener(), list[position])
            bind(createAnimalDetailListener(list[position]), list[position])
        }
    }

    private fun createFileClaimListener(body: Animal): View.OnClickListener {
        return View.OnClickListener {
            if (!body.registeredPolicyId.isNullOrEmpty()) {

                navigateActivityWithExtra(
                    VerifyAnimalActivity::class.java, context as OwnerHomeActivity,
                    Constants.NavigationKeys.ownerId, body.ownerId, false)
            }else{
                Toast.makeText(context, "Policy Not Registered!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun createAnimalDetailListener(body: Animal): View.OnClickListener {
        return View.OnClickListener {
                navigateActivityWithExtra(AnimalDetailActivity::class.java, context as OwnerHomeActivity,
                    Constants.NavigationKeys.animalId, body._id, false)
        }
    }

    inner class ViewHolder(private val binding: ItemAnimalBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(
            animalDetailListener: View.OnClickListener,
            body: Animal
        ) {
            with(binding) {
                try {
                    animalDetail = animalDetailListener
                    animal = body
                } catch (ex: Exception) {
                    Log.e("ADDED_ANIMAL_ADAP_ERROR","$ex")
                }
            }
        }
    }

    fun updateList(newList: List<Animal>) {
        (list as ArrayList).clear()
        (list as ArrayList).addAll(newList)
        notifyDataSetChanged()
    }

}