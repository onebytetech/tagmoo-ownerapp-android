package com.onebyte.tagmoo_owner.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.services.dataModels.otp.OtpResponse
import com.onebyte.tagmoo_owner.services.dataModels.otp.SendOtp
import com.onebyte.tagmoo_owner.services.dataModels.otp.VerifyOtp
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.User
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.services.repositories.Repository
import com.onebyte.tagmoo_owner.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LoginViewModel @Inject constructor() : BaseViewModel() {


    @Inject
    lateinit var repository: Repository
    val user = MutableLiveData(User())
    private val loginResponse: MutableLiveData<Any> = MutableLiveData()
    fun loginResponse(): LiveData<Any> = loginResponse

    val isLoading = MutableLiveData<Boolean>()

    private val otpResponse: MutableLiveData<Any> = MutableLiveData()
    fun otpResponse(): LiveData<Any> = otpResponse

    fun loginUser() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.loginUser(user.value!!)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        loginResponse.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: UserModel = gson.fromJson(
                                error,
                                UserModel::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            loginResponse.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                loginResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun verifyOtp(otp: String, token: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                val verifyOtp = VerifyOtp(otp)
                repository.verifyOtp(verifyOtp, token)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        otpResponse.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: UserModel = gson.fromJson(
                                error,
                                UserModel::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            otpResponse.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                otpResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun sendOtp() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                if(user.value!!.phoneNumber.substring(0, 1).equals("0"))
                    user.value!!.phoneNumber = "+92" + user.value!!.phoneNumber.substring(1, user.value!!.phoneNumber.length)
                val sendOtp = SendOtp(user.value!!.phoneNumber)
                repository.sendOtp(sendOtp)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        otpResponse.postValue(it.body())
                    else {
                        var error: String = it.errorBody()!!.string()
                        try {

                            val gson = Gson()
                            val errorModel: OtpResponse = gson.fromJson(
                                error,
                                OtpResponse::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            otpResponse.postValue(errorModel)

                        } catch (e: Exception) {
                            otpResponse.postValue(error)
                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                otpResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun loadDataCoroutine() {
    }
}