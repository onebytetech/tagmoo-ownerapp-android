package com.onebyte.tagmoo_owner.ui.takePictureView

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.onebyte.tagmoo_owner.services.repositories.Repository
import com.onebyte.tagmoo_owner.ui.base.BaseViewModel
import javax.inject.Inject

class TakePictureViewModel @Inject constructor() :
    BaseViewModel() {
    @Inject
    lateinit var repository: Repository

    private val uploadPictureResponse: MutableLiveData<Any> = MutableLiveData()
    val isLoading = MutableLiveData<Boolean>()

    fun uploadPictureResponse(): LiveData<Any> = uploadPictureResponse

    fun uploadImage(imageUrl: String, isCompare: Boolean) {
//        isLoading.postValue(true)
//        viewModelScope.launch(Dispatchers.IO) {
//
//            runCatching {
//                repository.uploadImage(imageUrl, isCompare)
//
//            }.onSuccess {
//                isLoading.postValue(false)
//
//                withContext(Dispatchers.Main) {
//                    if (it.isSuccessful)
//                        uploadPictureResponse.postValue(it.body())
//                    else {
//                        uploadPictureResponse.postValue(it.errorBody()!!.string())
//                        Log.e("ERROR_IMAGE_UPLOAD", it.errorBody()!!.string())
//                    }
//                }
//            }.onFailure {
//                isLoading.postValue(false)
//                uploadPictureResponse.postValue(it.message)
//                Log.e("ERROR_IMAGE_UPLOAD", it.message!!)
//
//            }
//        }
    }
}