package com.onebyte.tagmoo_owner.ui.add.fragments

import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentAddAnimalBinding
import com.onebyte.tagmoo_owner.databinding.FragmentAddPolicyBinding
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddViewModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_animal.tvNext
import kotlinx.android.synthetic.main.fragment_add_policy.*

class AddPolicyFragment : BaseFragment<FragmentAddPolicyBinding, AddViewModel>() {

    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override val layoutId: Int get()= R.layout.fragment_add_policy

    val TAG = AddPolicyFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        tvNext.setOnClickListener {
            val parent = (context as AddAnimalActivity)
            parent.binding.step = 2
            parent.loadFragment(parent.cameraFragment, parent.cameraFragment.TAG)
        }

        val list = mutableListOf("1 Policy","2 Policy","3 Policy", "4 Policy")
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spPolicy.adapter = adapter
    }
}
