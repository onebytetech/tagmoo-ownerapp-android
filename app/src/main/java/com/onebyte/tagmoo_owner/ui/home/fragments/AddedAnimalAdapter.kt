package com.onebyte.tagmoo_owner.ui.home.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.*
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ItemOwnerListBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import javax.inject.Inject

class AddedAnimalAdapter @Inject constructor(var animalImagesAdapter: AnimalImagesAdapter):
    RecyclerView.Adapter<AddedAnimalAdapter.ViewHolder>() {

    var list: List<AddedAnimal> = arrayListOf()

    override fun getItemCount(): Int {
//        return list.size
        return 5
    }

    var context: Context? = null
    private lateinit var onClick: OnClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_owner_list, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
//            bind(createAddClickListener(), list[position])
            bind(createAddClickListener())
        }
    }

    private fun createAddClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val pos = it.tag.toString().toInt()
            val animal = list[pos]
//            var count = (context as HomeActivity).addedAnimalFragment.getSuccessItem(animal)
//            if(animal.animalList.size < 10 || animal.countUploaded == animal.animalList.size) {
//                if (!(context as HomeActivity).addedAnimalFragment.isUploading)
//                    navigateActivityWithExtra(
//                        CameraActivity::class.java,
//                        context as Activity,
//                        Constants.PreferenceKeys.POS,
//                        pos,
//                        Constants.PreferenceKeys.COUNT,
//                        count,
//                        false
//                    )
//                else
//                    (context as HomeActivity).addedAnimalFragment.showUploadingMessage()
//            } else {
//                (context as HomeActivity).customPopups.showAlertPopup(
//                    context!!.getString(R.string.str_alert), context!!.getString(R.string.str_upload_all_images_message), context as HomeActivity, null)
//            }
        }
    }

    inner class ViewHolder(private val binding: ItemOwnerListBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(addClickListener: View.OnClickListener) {
            with(binding) {
                try {
                    clickAdd = addClickListener
                    tag = layoutPosition.toString()
                    isHide = false
//                    tvName.text = if(!addedAnimal.animal.isNullOrEmpty()) addedAnimal.animal else "Animal ${layoutPosition+1}"
                    rvAddedAnimal.layoutManager = (context as HomeActivity).component.assistedInjectionFactory.create(context!!)
                    val adapter = AddedAnimalsAdapter()
                    rvAddedAnimal.adapter = adapter
//                    adapter.submitList(addedAnimal.animalList)
                } catch (ex: Exception) {
                    Log.e("ADDED_ANIMAL_ADAP_ERROR","$ex")
                }
            }
        }
    }

    fun updateList(newList: List<AddedAnimal>, position: Int) {
        (list as ArrayList).clear()
        (list as ArrayList).addAll(newList)
        notifyItemChanged(position)
    }


    fun setOnClick(onClick: OnClick){
        this.onClick = onClick
    }

    interface OnClick{
        fun onClickUpload(pos: Int)
    }
}