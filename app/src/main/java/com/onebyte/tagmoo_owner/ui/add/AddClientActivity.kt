package com.onebyte.tagmoo_owner.ui.add

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.observe
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ActivityAddClientBinding
import com.onebyte.tagmoo_owner.databinding.ActivityHomeBinding
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_add_client.*
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class AddClientActivity : BaseActivity<ActivityAddClientBinding, AddViewModel>() {

    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_add_client

    var cal = Calendar.getInstance()

    @Inject
    lateinit var progressHUD: ProgressHUD

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        validatorInitializer()
        viewModelObservables()
    }

    private fun initViews() {

        // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateDateInView()
            }
        etDOB.setOnClickListener {
            DatePickerDialog(
                this,
                dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        tvSubmit.setOnClickListener {
            if (Validation.isConnected(this)){
                if (validator.validate()) viewModel.addOwner("Bearer "+PrefManager.loadPreferences(sp, Constants.PreferenceKeys.TOKEN)!!)
            }else{
                customPopups.showAlertPopup("", "error", this, null)
            }
        }

    }

    private fun validatorInitializer() {
        validator = component.validatorAssistedInjectionFactory.create(binding)
    }

    private fun viewModelObservables(){
        viewModel.apiResponse().observe(this){
            try {
                it.let {
                    val res = it as UserModel
                    if (res.status.code == 200) {

                        Log.e("Data", res.body.firstName)
                        navigateActivity(HomeActivity::class.java, this, true)
                        Toast.makeText(this, "Client Create Successfully!", Toast.LENGTH_LONG).show()
                    }else{
                        customPopups.showAlertPopup("", res.status.message, this, null)

                    }
                }
            } catch (ex: Exception) {
                it.let {
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }



    private fun updateDateInView() {
        val myFormat = "E, dd MMM yyyy HH:mm:ss z" // mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        etDOB.setText(sdf.format(cal.time))

        Log.e("Date", sdf.format(cal.time))
    }

}
