package com.onebyte.tagmoo_owner.ui.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.observe
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityAnimalDetailBinding
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ClaimModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalDetailModel
import com.onebyte.tagmoo_owner.services.dataModels.errorModel.Status
import com.onebyte.tagmoo_owner.services.dataModels.errorModel.StatusModel
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.detail.PolicyDetailActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_animal_detail.*

import java.lang.Exception
import javax.inject.Inject

class AnimalDetailActivity : BaseActivity<ActivityAnimalDetailBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    @Inject
    lateinit var progressHUD: ProgressHUD

    override fun layoutId(): Int = R.layout.activity_animal_detail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setListeners()
        initViews()
        viewModelObservables()
    }

    private fun setListeners() {
        tvFileClaim.setOnClickListener {
            if(binding.animal!!.claimStatus.isNullOrEmpty())
                navigateActivityWithExtra(
                    VerifyAnimalActivity::class.java, this,
                    Constants.NavigationKeys.ownerId, binding.animal!!.ownerId, false)
            else
                customPopups.showConfirmPopup(getString(R.string.str_alert), "Are you sure to cancel this claim?", this, onPopupClick)
        }
        binding.onClickPolicy = onClickPolicy

    }

    val onClickPolicy = View.OnClickListener {
        navigateActivityWithExtra(PolicyDetailActivity::class.java, this, Constants.PreferenceKeys.POLICY, Gson().toJson(binding.animal!!.registeredPolicyId), false)
    }

    private val onPopupClick = object : CustomPopups.OnClickConfirm{
        override fun onBackPress(isYes: Boolean) {
            if(isYes)
                viewModel.cancelClaim(binding.animal!!.claimId)
        }
    }

    private fun initViews() {
        val animalId = intent.getStringExtra(Constants.NavigationKeys.animalId)
        viewModel.getAnimal(animalId!!)
    }

    private fun viewModelObservables(){
        viewModel.getAnimalResponse().observe(this){
            try {
                it.let {
                    val res = it as AnimalDetailModel
                     if(res.body != null)
                        binding.animal = res.body
                }
            } catch (ex: Exception) {
                try {
                    customPopups.showAlertPopup("", Gson().fromJson(it.toString(), Status::class.java).message, this, null)
                } catch (ex: Exception){
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.cancelClaimResponse().observe(this){
           var msg = ""
            try {
                it.let {
                    val res = it as ClaimModel
                    if(res.status.code == 200) {
                        msg = res.status.message
                        binding.animal!!.claimStatus = null
                    }
                }
            } catch (ex: Exception) {
                msg = try {
                    Gson().fromJson(it.toString(), StatusModel::class.java).status!!.message
                } catch (ex: Exception){
                    it.toString()
                }
            }
            if(msg == null)
                msg = "Error"
            customPopups.showAlertPopup("", msg, this, null)

        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }

}
