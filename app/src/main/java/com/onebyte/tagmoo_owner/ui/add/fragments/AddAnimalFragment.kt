package com.onebyte.tagmoo_owner.ui.add.fragments

import android.os.Bundle
import android.view.View
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentAddAnimalBinding
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddViewModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_animal.*

class AddAnimalFragment : BaseFragment<FragmentAddAnimalBinding, AddViewModel>() {

    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override val layoutId: Int get()= R.layout.fragment_add_animal

    val TAG = AddAnimalFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() {
        tvNext.setOnClickListener {
            val parent = (context as AddAnimalActivity)
            parent.binding.step = 2
            parent.loadFragment(parent.cameraFragment, parent.cameraFragment.TAG)
        }
    }
}
