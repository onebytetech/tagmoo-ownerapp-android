package com.onebyte.tagmoo_owner.ui.contact

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.onebyte.tagmoo_owner.services.dataModels.contact.Contact
import com.onebyte.tagmoo_owner.services.repositories.Repository
import com.onebyte.tagmoo_owner.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ContactViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var repository: Repository

    val contactResponse: MutableLiveData<Any> = MutableLiveData()
    val isLoading = MutableLiveData<Boolean>()

    fun contact(contact: Contact) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.contact(contact)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        contactResponse.postValue(it.body())
                    else {
                        contactResponse.postValue(it.errorBody()!!.string())
                        Log.e("ERROR_IMAGE_UPLOAD", it.errorBody()!!.string())
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                contactResponse.postValue(it.message)
                Log.e("ERROR_IMAGE_UPLOAD", it.message!!)

            }
        }
    }
}