package com.onebyte.tagmoo_owner.ui.add

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityAddAnimalBinding
import com.onebyte.tagmoo_owner.ui.add.fragments.AddAnimalFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.AddPolicyFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CameraFragment
import com.onebyte.tagmoo_owner.ui.add.fragments.CapturedImagesFragment
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_add_animal.*
import javax.inject.Inject

class AddAnimalActivity : BaseActivity<ActivityAddAnimalBinding, AddViewModel>() {

    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_add_animal

    @Inject
    lateinit var cameraFragment: CameraFragment

    @Inject
    lateinit var addAnimalFragment: AddAnimalFragment

    @Inject
    lateinit var capturedImagesFragment: CapturedImagesFragment

    @Inject
    lateinit var addPolicyFragment: AddPolicyFragment

    var isAddMore = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()
        setListeners()
    }

    private fun setListeners() {
        ivBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initViews() {
        loadFragment(addAnimalFragment, addAnimalFragment.TAG)
        binding.step = 1
    }

    fun loadFragment(fragment: Fragment, tag: String){
        supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .setCustomAnimations(R.anim.slide_up, R.anim.slide_down)
            .replace(R.id.flAddAnimal, fragment, tag)
            .commit()
    }

    override fun onBackPressed() {
        when(binding.step) {
            1 -> finish()
            2 -> {
                binding.step = 1
                onFragmentDetached(cameraFragment.TAG)
            }
            3 -> {
                binding.step = 1
                onFragmentDetached(capturedImagesFragment.TAG)
            }
            4 -> {
                binding.step = 3
                onFragmentDetached(addPolicyFragment.TAG)
            }
        }
    }
}
