package com.onebyte.tagmoo_owner.ui.home.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ItemAnimalImagesBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages

class AnimalImagesAdapter: RecyclerView.Adapter<AnimalImagesAdapter.ViewHolder>() {

    val list: List<AnimalImages> = arrayListOf()

    override fun getItemCount(): Int {
        return list.size
    }

    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_animal_images, parent, false
            )
        )
    }

    fun updateList(newList: List<AnimalImages>, position: Int) {
        (list as ArrayList).clear()
        for (item: AnimalImages in newList){
            if(item.isUploaded)
                list.add(item)
        }
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            bind(list[position])
        }
    }

    inner class ViewHolder(private val binding: ItemAnimalImagesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(animalImages: AnimalImages) {
            with(binding) {
                try {
                    tag = layoutPosition.toString()
                    animalImage = animalImages
                } catch (ex: Exception) {
                }
            }
        }
    }
}