package com.onebyte.tagmoo_owner.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.Claim
import com.onebyte.tagmoo_owner.services.dataModels.ClaimModel.ClaimModel
import com.onebyte.tagmoo_owner.services.dataModels.animal.AnimalModels
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.services.repositories.Repository
import com.onebyte.tagmoo_owner.ui.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeViewModel @Inject constructor() : BaseViewModel() {

    @Inject
    lateinit var repository: Repository

    private val subAgentOwnersList: MutableLiveData<Any> = MutableLiveData()
    private val claimApiResponse: MutableLiveData<Any> = MutableLiveData()
    private val ownerAnimalList: MutableLiveData<Any> = MutableLiveData()
    private val getAnimalResponse: MutableLiveData<Any> = MutableLiveData()
    private val cancelClaimResponse: MutableLiveData<Any> = MutableLiveData()
    private val uploadImageResponse: MutableLiveData<Any> = MutableLiveData()

    val claim = MutableLiveData(Claim())

    fun subAgentOwnersList(): LiveData<Any> = subAgentOwnersList
    fun claimApiResponse(): LiveData<Any> = claimApiResponse
    fun ownerAnimalListApiResponse(): LiveData<Any> = ownerAnimalList
    fun getAnimalResponse(): LiveData<Any> = getAnimalResponse
    fun cancelClaimResponse(): LiveData<Any> = cancelClaimResponse
    fun uploadImageResponse(): LiveData<Any> = uploadImageResponse


    private val uploadPictureResponse: MutableLiveData<Any> = MutableLiveData()
    val isLoading = MutableLiveData<Boolean>()

    fun uploadPictureResponse(): LiveData<Any> = uploadPictureResponse

    fun uploadImage(imageUrl: String, ownerId: String, isCompare: Boolean, token: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.uploadImageToCompare(imageUrl, ownerId, isCompare, token)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        uploadPictureResponse.postValue(it.body())
                    else {
                        uploadPictureResponse.postValue(it.errorBody()!!.string())
                        Log.e("ERROR_IMAGE_UPLOAD", it.errorBody()!!.string())
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                uploadPictureResponse.postValue(it.message)
                Log.e("ERROR_IMAGE_UPLOAD", it.message!!)

            }
        }
    }

    fun uploadImage(imageUrl: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.uploadImage(imageUrl)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        uploadImageResponse.postValue(it.body())
                    else {
                        uploadImageResponse.postValue(it.errorBody()!!.string())
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                uploadImageResponse.postValue(it.message)
                Log.e("ERROR_IMAGE_UPLOAD", it.message!!)

            }
        }
    }

    fun subAgentOwnerList(token: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.getSubAgentOwners(token)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        subAgentOwnersList.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: UserModel = gson.fromJson(
                                error,
                                UserModel::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            subAgentOwnersList.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                subAgentOwnersList.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun ownerAnimalList(disableLoader: Boolean) {
        if (!disableLoader)
            isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.getAnimals()

            }.onSuccess {
                if (!disableLoader)
                    isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        ownerAnimalList.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: AnimalModels = gson.fromJson(
                                error,
                                AnimalModels::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            ownerAnimalList.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                if (!disableLoader)
                    isLoading.postValue(false)
                ownerAnimalList.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun ownerAnimalList(isSearch: Boolean, filter: String) {
        if (!isSearch)
            isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.getAnimals(isSearch, filter)

            }.onSuccess {
                if (!isSearch)
                    isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        ownerAnimalList.postValue(it.body())
                    else {
                        try {
                            ownerAnimalList.postValue(it.errorBody()!!.string())
                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                if (!isSearch)
                    isLoading.postValue(false)
                ownerAnimalList.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)

            }
        }
    }

    fun fileClaim() {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.fileClaim(claim.value!!)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        claimApiResponse.postValue(it.body())
                    else {
                        try {
                            var error: String = it.errorBody()!!.string()

                            val gson = Gson()
                            val errorModel: ClaimModel = gson.fromJson(
                                error,
                                ClaimModel::class.java
                            )

                            Log.e("LOGIN_USER_ERROR", errorModel.status.message)
                            claimApiResponse.postValue(errorModel)

                        } catch (e: Exception) {

                        }
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                claimApiResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)


            }
        }
    }

    fun getAnimal(animalId: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.getAnimal(animalId)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        getAnimalResponse.postValue(it.body())
                    else {
                        getAnimalResponse.postValue(it.errorBody()!!.string())
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                getAnimalResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)
            }
        }
    }

    fun cancelClaim(claimId: String) {
        isLoading.postValue(true)
        viewModelScope.launch(Dispatchers.IO) {

            runCatching {
                repository.cancelClaim(claimId)

            }.onSuccess {
                isLoading.postValue(false)

                withContext(Dispatchers.Main) {
                    if (it.isSuccessful)
                        cancelClaimResponse.postValue(it.body())
                    else {
                        cancelClaimResponse.postValue(it.errorBody()!!.string())
                    }
                }
            }.onFailure {
                isLoading.postValue(false)
                cancelClaimResponse.postValue(it.message)
                Log.e("LOGIN_USER_ERROR", it.message!!)
            }
        }
    }
}