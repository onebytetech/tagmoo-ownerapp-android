package com.onebyte.tagmoo_owner.ui.login

import android.os.Bundle
import androidx.lifecycle.observe
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ActivityOwnerLoginBinding
import com.onebyte.tagmoo_owner.services.dataModels.otp.OtpResponse
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.forgotPassword.ForgotPasswordActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_owner_login.*
import java.lang.Exception
import javax.inject.Inject


class OwnerLoginActivity :  BaseActivity<ActivityOwnerLoginBinding, LoginViewModel>() {

    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_owner_login

    @Inject
    lateinit var progressHUD: ProgressHUD

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        validatorInitializer()
        viewModelObservables()
        setListeners()
    }

    private fun validatorInitializer() {
        validator = component.validatorAssistedInjectionFactory.create(binding)
    }

    private fun viewModelObservables(){
        viewModel.otpResponse().observe(this){
            try {
                it.let {
                    val res = it as OtpResponse
                    if (res.status.code == 200) {
                        navigateActivityWithExtra(OtpActivity::class.java, this@OwnerLoginActivity, Constants.PreferenceKeys.TOKEN, it.body,false)
                    }else{
                        customPopups.showAlertPopup("", res.status.message, this, null)
                    }
                }
            } catch (ex: Exception) {
                it.let {
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }


    private fun setListeners() {


        tvForgotPassword.setOnClickListener{
            navigateActivity(ForgotPasswordActivity::class.java, this, false)
        }


        tvLogin.setOnClickListener{
            if (Validation.isConnected(this)){
//                etPhone.setText("+923336685600")
                if (validator.validate())
                    viewModel.sendOtp()
            }
        }

    }

}
