package com.onebyte.tagmoo_owner.ui.home.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.FragmentHomeBinding
import com.onebyte.tagmoo_owner.databinding.FragmentListingBinding
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.OwnerModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.fragment_listing.*
import java.lang.Exception
import javax.inject.Inject

class ListingFragment : BaseFragment<FragmentListingBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_listing

    val TAG = ListingFragment::class.java.simpleName

    @Inject
    lateinit var ownerListAdapter: OwnerListAdapter

    lateinit var validator: Validator

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        validatorInitializer()
        setListeners()
    }

    private fun setListeners() {
        tvAll.setOnClickListener {
            binding.selected = 1
        }
        tvIncomplete.setOnClickListener {
            binding.selected = 2
        }
        tvPending.setOnClickListener {
            binding.selected = 3
        }
        tvClaimed.setOnClickListener {
            binding.selected = 4
        }
    }


    private fun validatorInitializer() {
        validator = (context as HomeActivity).component.validatorAssistedInjectionFactory.create(binding)
    }

    private fun initViews() {
        binding.selected = 1

        val list = mutableListOf("Ali","Umar","Abubakar", "Fahad")
        val adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, list)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spOwner.adapter = adapter

        rvOwner.layoutManager = LinearLayoutManager(context)
        rvOwner.adapter = ownerListAdapter
        if (Validation.isConnected(activity!!)){

            viewModel!!.subAgentOwnerList("Bearer "+ PrefManager.loadPreferences((context as HomeActivity).sp, Constants.PreferenceKeys.TOKEN)!!)
        }
        viewModelObservables()
    }

    private fun viewModelObservables(){
        viewModel!!.subAgentOwnersList().observe(this){
            try {
                it.let {
                    val res = it as OwnerModel
                    if (res.status.code == 200) {

                        Log.e("DataList", "========>"+res.body.size)
                        ownerListAdapter.updateList(res.body)
//                        navigateActivity(HomeActivity::class.java, this, true)
                    }else{
                        (context as HomeActivity).customPopups.showAlertPopup("", res.status.message, activity!!, null)

                    }
                }
            } catch (ex: Exception) {
                it.let {
                    (context as HomeActivity).customPopups.showAlertPopup("", ex.message.toString(), activity!!, null)
                }
            }
        }

        viewModel!!.isLoading.observe(this) {
            if (it) {
                showLoader((context as HomeActivity).progressHUD)
            } else
                hideLoader((context as HomeActivity).progressHUD)
        }

    }
}
