package com.onebyte.tagmoo_owner.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentFinancialBinding
import com.onebyte.tagmoo_owner.databinding.FragmentHomeBinding
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.home.HomeViewModel

class FinancialFragment : BaseFragment<FragmentFinancialBinding, HomeViewModel>() {
    override fun getViewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_financial

    val TAG = FinancialFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }


    private fun initViews() {
    }
}
