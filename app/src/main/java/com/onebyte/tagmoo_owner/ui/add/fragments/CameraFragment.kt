package com.onebyte.tagmoo_owner.ui.add.fragments

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Matrix
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.Size
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.onebyte.tagmoo_owner.BuildConfig
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.FragmentCameraBinding
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AddedAnimal
import com.onebyte.tagmoo_owner.services.dataModels.addedAnimalModels.AnimalImages
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.add.AddViewModel
import com.onebyte.tagmoo_owner.ui.base.BaseFragment
import com.onebyte.tagmoo_owner.ui.cameraView.LuminosityAnalyzer
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.CustomPopups
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.ProgressHUD
import kotlinx.android.synthetic.main.fragment_camera.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.concurrent.Executors

class CameraFragment : BaseFragment<FragmentCameraBinding, AddViewModel>() {
    override fun getViewModelClass(): Class<AddViewModel> = AddViewModel::class.java

    override val layoutId: Int get() = R.layout.fragment_camera

    val TAG = CameraFragment::class.java.simpleName
    private val CHOOSE_GALLERY_IMAGE = 2
    private val PIC_CROP = 1
    private val REQUEST_CODE_PERMISSIONS = 10
    private val READ_WRITE_PER_CODE = 11
    private val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    private var pos = -1
    lateinit var adapter: CameraImagesAdapter
    var mCropImagedUri : Uri? = null


    lateinit var progressHUD: ProgressHUD

    lateinit var urlList: ArrayList<AnimalImages>
    lateinit var addMoreList: ArrayList<AnimalImages>


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }


    private fun initViews() {
        if (allPermissionsGranted()) {
            cameraView.post { startCamera() }
        } else {
            ActivityCompat.requestPermissions(
                context as AddAnimalActivity, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS)
        }
        progressHUD = ProgressHUD(context!!)
        urlList = arrayListOf()
        addMoreList = arrayListOf()
        adapter = CameraImagesAdapter()
        rvImages.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvImages.adapter = adapter
        setListeners()
        val parent = (context as AddAnimalActivity)
        if(parent.isAddMore) {
            val listAll = PrefManager.loadNewAnimalListPreferences(parent.sp)
            val listAddMore = getAddMoreList(listAll[listAll.size - 1].animalList as ArrayList<AnimalImages>)
            urlList.addAll(listAddMore)
            adapter.updateList(urlList, 0)
            binding.count = listAddMore.size
        }
    }

    private fun getAddMoreList(arrayList: java.util.ArrayList<AnimalImages>): java.util.ArrayList<AnimalImages> {
        val list = arrayListOf<AnimalImages>()
        for(item in arrayList)
            if(!item.isUploaded)
                list.add(item)
        return list
    }


    private val executor = Executors.newSingleThreadExecutor()

    private fun setListeners() {
//        ivGallery.setOnClickListener{
//            if(!checkExternalPermissionGranted())
//                requestExternalPermission()
//        }

        tvFinish.setOnClickListener {
            val parent = (context as AddAnimalActivity)
                if(parent.isAddMore) {
                    if (addMoreList.isNotEmpty()) {
                        PrefManager.saveNewAnimalListPreferences(
                            AddedAnimal(
                                System.currentTimeMillis(), "",
                                isUploaded = false,
                                isUploading = false,
                                countUploaded = 0,
                                animalList = addMoreList
                            ), (context as AddAnimalActivity).sp, 0
                        )
                        parent.binding.step = 3
                        parent.onFragmentDetached(TAG)
                        parent.loadFragment(parent.capturedImagesFragment, parent.capturedImagesFragment.TAG)
                    } else {
                        parent.customPopups.showAlertPopup(getString(R.string.str_alert), "Please select at least one image.", parent, null)
                    }
                } else {
                    if(urlList.isNotEmpty()) {
                        PrefManager.saveNewAnimalListPreferences(
                            AddedAnimal(
                                System.currentTimeMillis(), "",
                                isUploaded = false,
                                isUploading = false,
                                countUploaded = 0,
                                animalList = urlList
                            ), (context as AddAnimalActivity).sp, pos
                        )
                        parent.binding.step = 3
                        parent.onFragmentDetached(TAG)
                        parent.loadFragment(parent.capturedImagesFragment, parent.capturedImagesFragment.TAG)
                    } else {
                        parent.customPopups.showAlertPopup(getString(R.string.str_alert), "Please select at least one image.", parent, null)
                    }
                }
        }

        cameraView.addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
            updateTransform()
        }
    }


    private fun requestExternalPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val permissions = arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            requestPermissions(permissions, READ_WRITE_PER_CODE)
        }
    }

    private fun checkExternalPermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((context as AddAnimalActivity).checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && (context as AddAnimalActivity).checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                (context as AddAnimalActivity).customPopups.showAlertPopup(getString(R.string.str_alert),getString(R.string.str_gallery_images_detail), context as AddAnimalActivity, onGalleryClick)
                true
            } else {
                false
            }
        } else
            true
    }

    private val onGalleryClick = object: CustomPopups.OnClick{
        override fun onBackPress() {
            val picImageIntent = getImageIntent(true, null)
            startActivityForResult(picImageIntent, CHOOSE_GALLERY_IMAGE)
        }
    }

    private fun getImageIntent(isGallery: Boolean, picUri: Uri?): Intent {
        val cropIntent: Intent
        if(isGallery) {
            cropIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            cropIntent.type = "image/*"
        } else {
            cropIntent = Intent("com.android.camera.action.CROP")
            cropIntent.setDataAndType(picUri, "image/*")
            cropIntent.putExtra("crop", true)

            val f = createNewFile()
            try {
                f.createNewFile()
            } catch (ex: IOException) {
                Log.e("IO_EXCEPTION", "$ex")
            }

            mCropImagedUri = Uri.fromFile(f)
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri)
        }
        cropIntent.putExtra("aspectX", 3)
        cropIntent.putExtra("aspectY", 2)
        cropIntent.putExtra("scale", true)
        cropIntent.putExtra("outputX", 2048)
        cropIntent.putExtra("outputY", 1365)
        cropIntent.putExtra("return-data", false)
        cropIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        cropIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

        return cropIntent
    }

    private fun performCrop(picUri: Uri) {
        try {
            val cropIntent = getImageIntent(false, picUri)
            startActivityForResult(cropIntent, PIC_CROP)
        } catch (aNFE: ActivityNotFoundException) {
            Toast.makeText(context, getString(R.string.str_crop_device_message), Toast.LENGTH_SHORT).show()
            PrefManager.savePreferences(Constants.PreferenceKeys.imageUrl, picUri.path.toString() , (context as AddAnimalActivity).sp)
        }
    }

    private fun createNewFile() : File {
        val newDirectory : File = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).absolutePath)
        }else {
            File((context as AddAnimalActivity).getExternalFilesDir(null)!!.absolutePath)
        }
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d("DIRECTORY_PATH", newDirectory.absolutePath)
            }
        }
        val file = File(newDirectory, ("CROP_" + System.currentTimeMillis() + ".jpg"))
        if (file.exists()) {

            file.delete()
            try {
                file.createNewFile()
            } catch (e: IOException) {
                Log.e("NEW_FILE_CREATE_ERROR","$e")
            }
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
//            if (data != null && data.data != null) {
            if(resultCode == Activity.RESULT_OK && requestCode == 1) {
                if (data != null) {
                    var imagePath = data.data.toString()
                    if (data.data == null) {
                        imagePath = mCropImagedUri.toString()
                    }
//                var imagePath = data.data.toString()
                    when (requestCode) {
                        CHOOSE_GALLERY_IMAGE -> {
                            val proj = arrayOf("_data")
                            val cursor = (context as AddAnimalActivity).contentResolver.query(
                                data.data!!,
                                proj,
                                null,
                                null,
                                null
                            )
                            if (cursor != null) {
                                if (cursor.moveToFirst()) {
                                    val columnIndex = cursor.getColumnIndexOrThrow(proj[0])
                                    imagePath = cursor.getString(columnIndex)
                                }
                                cursor.close()
                            }
                        }
                    }
                    urlList.add(
                        AnimalImages(
                            0, imagePath.replace("file:", ""), false,
                            isUploaded = false
                        )
                    )
                    binding!!.count = urlList.size
                    (context as AddAnimalActivity).hideLoader(progressHUD)
                    adapter.updateList(urlList, urlList.size - 1)
                    if ((context as AddAnimalActivity).isAddMore)
                        addMoreList.add(
                            AnimalImages(
                                0, imagePath.replace("file:", ""), false,
                                isUploaded = false
                            )
                        )
//                ivShow.visibility = View.VISIBLE
//                ivShow.loadImage(imagePath)
//                PrefManager.savePreferences(Constants.PreferenceKeys.imageUrl, imagePath
//                    .replace("file:",""), sp)
//                finish()
                }
            }
        } catch (ex: Exception){
            Log.e("CAMERA_IMAGE_ERROR", "$ex")
        }
    }

    private fun startCamera() {
        val previewConfig = PreviewConfig.Builder().apply {
            setTargetResolution(Size(1024, 768))
        }.build()

        val preview = Preview(previewConfig)
        preview.setOnPreviewOutputUpdateListener {

            val parent = cameraView.parent as ViewGroup
            parent.removeView(cameraView)
            parent.addView(cameraView, 0)

            cameraView.surfaceTexture = it.surfaceTexture
            updateTransform()
        }


        val imageCaptureConfig = ImageCaptureConfig.Builder()
            .apply {
                setCaptureMode(ImageCapture.CaptureMode.MIN_LATENCY)
            }.build()

        val imageCapture = ImageCapture(imageCaptureConfig)

        ivCapture.setOnClickListener {
            (context as AddAnimalActivity).showLoader(progressHUD)
            val file = File((context as AddAnimalActivity).externalMediaDirs.first(),
                "${System.currentTimeMillis()}.jpg")

            imageCapture.takePicture(file, executor,
                object : ImageCapture.OnImageSavedListener {
                    override fun onError(
                        imageCaptureError: ImageCapture.ImageCaptureError,
                        message: String,
                        exc: Throwable?) {

                        val msg = "Photo capture failed: $message"
                        Log.e("CameraXApp", msg, exc)
                        cameraView.post { Toast.makeText(context, msg, Toast.LENGTH_SHORT).show() }
                        (context as AddAnimalActivity).hideLoader(progressHUD)
                    }

                    override fun onImageSaved(file: File) {
                        cameraView.post {
                            performCrop(FileProvider.getUriForFile(context!!, BuildConfig.APPLICATION_ID + ".provider", file))
//                                cropImage(Uri.fromFile(file))
                        }
                    }
                })
        }

        val analyzerConfig = ImageAnalysisConfig.Builder().apply {
            setImageReaderMode(
                ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
        }.build()

        val analyzerUseCase = ImageAnalysis(analyzerConfig).apply {
            setAnalyzer(executor, LuminosityAnalyzer())
        }

        CameraX.bindToLifecycle(this, preview, imageCapture, analyzerUseCase)
    }

    fun cropImage(imageUri: Uri){
        try {
            MainScope().launch {
                val bitmap = MediaStore.Images.Media.getBitmap((context as AddAnimalActivity).contentResolver, imageUri)
                val matrix = Matrix()
                matrix.postRotate(90f)

                val width = bitmap.width
                val height = bitmap.height
                var newWidth = if ((height > width)) width else height
                var newHeight = if ((height > width)) height - (height - width) else height
                newWidth -= 530
                newHeight -= 200
                val cropImg = Bitmap.createBitmap(bitmap, 400, 0, newWidth, newHeight)

                val rotatedBitmap =
                    Bitmap.createBitmap(cropImg, 0, 0, cropImg.width, cropImg.height, matrix, true)

                try {
                    FileOutputStream(File(imageUri.path!!)).use { out ->
                        rotatedBitmap.compress(Bitmap.CompressFormat.PNG, 100, out)
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                withContext(Dispatchers.Main){
                    //                    ivShow.visibility = View.VISIBLE
//                    binding.path = imageUri.path
                    (context as AddAnimalActivity).hideLoader(progressHUD)
                }
            }

        } catch (ex: Exception){
            (context as AddAnimalActivity).hideLoader(progressHUD)
            Log.e("IMAGE_CROP_ERROR", "$ex")
            (context as AddAnimalActivity).customPopups.showAlertPopup(getString(R.string.str_alert), getString(R.string.str_auto_crop_message), context as AddAnimalActivity, null)
        }
    }

    private fun updateTransform() {
        val matrix = Matrix()

        val centerX = cameraView.width / 2f
        val centerY = cameraView.height / 2f

        val rotationDegrees = when(cameraView.display.rotation) {
            Surface.ROTATION_0 -> 0
            Surface.ROTATION_90 -> 90
            Surface.ROTATION_180 -> 180
            Surface.ROTATION_270 -> 270
            else -> return
        }
        matrix.postRotate(-rotationDegrees.toFloat(), centerX, centerY)

        cameraView.setTransform(matrix)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_PERMISSIONS -> if (allPermissionsGranted()) {
                cameraView.post { startCamera() }
            } else {
                (context as AddAnimalActivity).customPopups.showAlertPopup(getString(R.string.str_camera_permission_title),
                    getString(R.string.str_camera_permission_detail), context as AddAnimalActivity, onClick)
            }
            READ_WRITE_PER_CODE -> {
                if(!checkExternalPermissionGranted()){
                    (context as AddAnimalActivity).customPopups.showAlertPopup(getString(R.string.str_external_permission_title),
                        getString(R.string.str_external_permission_detail), context as AddAnimalActivity, onClickExternalPermission)
                }
            }
        }
    }

    private val onClick = object: CustomPopups.OnClick{
        override fun onBackPress() {
            initViews()
        }
    }

    private val onClickExternalPermission = object: CustomPopups.OnClick{
        override fun onBackPress() {
            requestExternalPermission()
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            context!!, it) == PackageManager.PERMISSION_GRANTED
    }


}
