package com.onebyte.tagmoo_owner.ui.home.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.*
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ItemOwnerBinding
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.Body
import com.onebyte.tagmoo_owner.ui.add.AddAnimalActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.fileClaim.VerifyAnimalActivity
import com.onebyte.tagmoo_owner.utils.Constants
import com.onebyte.tagmoo_owner.utils.navigateActivity
import com.onebyte.tagmoo_owner.utils.navigateActivityWithExtra
import javax.inject.Inject

class OwnerListAdapter @Inject constructor(var animalImagesAdapter: AnimalImagesAdapter):
    RecyclerView.Adapter<OwnerListAdapter.ViewHolder>() {

    var list: List<Body> = arrayListOf()

    override fun getItemCount(): Int {
        return list.size
    }

    var context: Context? = null
    private lateinit var onClick: OnClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_owner, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
//            bind(createAddClickListener(), list[position])
            bind(createAddClickListener(list[position]), list[position])
        }
    }

    private fun createAddClickListener(body: Body): View.OnClickListener {
        return View.OnClickListener {
            if(body.animalIds.isNullOrEmpty()){

                navigateActivity(AddAnimalActivity::class.java, context as HomeActivity, false)
            }else{

                navigateActivityWithExtra(
                    VerifyAnimalActivity::class.java, context as HomeActivity,
                    Constants.NavigationKeys.ownerId, body._id, false)
            }

//            var count = (context as HomeActivity).addedAnimalFragment.getSuccessItem(animal)
//            if(animal.animalList.size < 10 || animal.countUploaded == animal.animalList.size) {
//                if (!(context as HomeActivity).addedAnimalFragment.isUploading)
//                    navigateActivityWithExtra(
//                        CameraActivity::class.java,
//                        context as Activity,
//                        Constants.PreferenceKeys.POS,
//                        pos,
//                        Constants.PreferenceKeys.COUNT,
//                        count,
//                        false
//                    )
//                else
//                    (context as HomeActivity).addedAnimalFragment.showUploadingMessage()
//            } else {
//                (context as HomeActivity).customPopups.showAlertPopup(
//                    context!!.getString(R.string.str_alert), context!!.getString(R.string.str_upload_all_images_message), context as HomeActivity, null)
//            }
        }
    }

    inner class ViewHolder(private val binding: ItemOwnerBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(
            addClickListener: View.OnClickListener,
            body: Body
        ) {
            with(binding) {
                try {
                    clickAdd = addClickListener
                    tag = layoutPosition.toString()
                    isHide = true
                    owner = body


                } catch (ex: Exception) {
                    Log.e("ADDED_ANIMAL_ADAP_ERROR","$ex")
                }
            }
        }
    }

    fun updateList(newList: List<Body>) {
        (list as ArrayList).clear()
        (list as ArrayList).addAll(newList)
        notifyDataSetChanged()
    }


    fun setOnClick(onClick: OnClick){
        this.onClick = onClick
    }

    interface OnClick{
        fun onClickUpload(pos: Int)
    }
}