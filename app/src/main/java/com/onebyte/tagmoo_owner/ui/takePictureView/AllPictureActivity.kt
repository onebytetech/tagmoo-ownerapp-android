package com.onebyte.tagmoo_owner.ui.takePictureView

import android.os.Bundle
import android.util.Log
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityAllPictureBinding
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.utils.*
import com.onebyte.tagmoo_owner.utils.Constants.PreferenceKeys.IMAGE_URLS
import kotlinx.android.synthetic.main.activity_all_picture.*
import kotlinx.android.synthetic.main.activity_take_picture.tvSubmit
import java.lang.IndexOutOfBoundsException

@Suppress("SENSELESS_COMPARISON")
class AllPictureActivity : BaseActivity<ActivityAllPictureBinding, TakePictureViewModel>() {
    private var imageUrls = arrayListOf<String>()

    override fun getViewModelClass(): Class<TakePictureViewModel> = TakePictureViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_all_picture

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setListeners()
        initView()

    }
    private fun initView() {
        binding.isCompleted = false
        PrefManager.removePrefrence(sp, IMAGE_URLS)
        Constants.animal = ""
    }

    private fun setListeners() {
        tvSubmit.setOnClickListener {
            if (!binding.isCompleted!!) {
                navigateActivity(TakePictureActivity::class.java, this, false)
            } else {
                customPopups.showAlertPopup("Successfully Uploaded", "You have successfully uploaded all the animal's muzzle pictures.\n" +
                        "Your animal's Id:" + Constants.animal, this, onClickPopup)
            }
        }
    }

    private val onClickPopup = object : CustomPopups.OnClick{
        override fun onBackPress() {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        imageUrls.clear()
        imageUrls.addAll(PrefManager.loadListImageUrlsPreferences(sp))
        binding.remainingImages = 10 - imageUrls.size
        try {
            ImageLoadAdapter.loadImage(ivImage1, imageUrls[0])
            ImageLoadAdapter.loadImage(ivImage2, imageUrls[1])
            ImageLoadAdapter.loadImage(ivImage3, imageUrls[2])
            ImageLoadAdapter.loadImage(ivImage4, imageUrls[3])
            ImageLoadAdapter.loadImage(ivImage5, imageUrls[4])
            ImageLoadAdapter.loadImage(ivImage6, imageUrls[5])
            ImageLoadAdapter.loadImage(ivImage7, imageUrls[6])
            ImageLoadAdapter.loadImage(ivImage8, imageUrls[7])
            ImageLoadAdapter.loadImage(ivImage9, imageUrls[8])
            ImageLoadAdapter.loadImage(ivImage10, imageUrls[9])
            binding.isCompleted = true
        } catch (ex: IndexOutOfBoundsException) {
            Log.e("IndexOutOfBoundEx", "$ex")
        }
    }
}
