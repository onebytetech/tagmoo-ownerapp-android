package com.onebyte.tagmoo_owner.ui.login

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.observe
import br.com.ilhasoft.support.validation.Validator
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.application.component
import com.onebyte.tagmoo_owner.databinding.ActivityLoginBinding
import com.onebyte.tagmoo_owner.services.dataModels.userOwner.UserModel
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.forgotPassword.ForgotPasswordActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import java.lang.Exception
import javax.inject.Inject

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {
    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_login

    @Inject
    lateinit var progressHUD: ProgressHUD

    lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        validatorInitializer()
        viewModelObservables()
        setListeners()
    }

    private fun validatorInitializer() {
        validator = component.validatorAssistedInjectionFactory.create(binding)
    }

    private fun viewModelObservables(){
        viewModel.loginResponse().observe(this){
            try {
                it.let {
                    val res = it as UserModel
                    if (res.status.code == 200) {

                        Log.e("Data", res.body.user.firstName)
                        PrefManager.savePreferences(Constants.PreferenceKeys.TOKEN, "Bearer " + res.body.user.token, sp)
                        navigateActivity(HomeActivity::class.java, this@LoginActivity, true)
                    }else{
                        customPopups.showAlertPopup("", res.status.message, this, null)

                    }
                }
            } catch (ex: Exception) {
                it.let {
                    customPopups.showAlertPopup("", ex.message.toString(), this, null)
                }
            }
        }

        viewModel.isLoading.observe(this) {
            if (it) {
                showLoader(progressHUD)
            } else
                hideLoader(progressHUD)
        }

    }


    private fun setListeners() {


        tvForgotPassword.setOnClickListener{
            navigateActivity(ForgotPasswordActivity::class.java, this, false)
        }


        tvLogin.setOnClickListener{
            if (Validation.isConnected(this)){
                etEmail.setText("3333333335333")
                etPassword.setText("33333333339")
                if (validator.validate()) viewModel.loginUser()
            }else{
                customPopups.showAlertPopup("", "error", this, null)
            }
        }

    }
}
