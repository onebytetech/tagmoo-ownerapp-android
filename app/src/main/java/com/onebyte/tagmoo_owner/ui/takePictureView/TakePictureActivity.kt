package com.onebyte.tagmoo_owner.ui.takePictureView

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.observe
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityTakePictureBinding
import com.onebyte.tagmoo_owner.services.dataModels.uploadPictureModels.UploadPictureResponse
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.cameraView.CameraActivity
import com.onebyte.tagmoo_owner.utils.*
import kotlinx.android.synthetic.main.activity_take_picture.*
import java.lang.Exception

@Suppress("SENSELESS_COMPARISON")
class TakePictureActivity : BaseActivity<ActivityTakePictureBinding, TakePictureViewModel>() {
    private var imageUrl = ""

    override fun getViewModelClass(): Class<TakePictureViewModel> = TakePictureViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_take_picture

    lateinit var progressHUD: ProgressHUD

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setListeners()
        getIntentData()
        viewModelObservable()
        initView()
    }

    private fun viewModelObservable() {
        viewModel.uploadPictureResponse().observe(this) {
            try {
                val res = it as UploadPictureResponse
                if (res.body != null && res.body != null) {
                    if (binding.isCompare!!) {
                        val msg: String
                        val title: String
                        if(res.body.animalId == null){
                            title = "Failed"
                            msg = res.status.message
                        } else {
                            title = "Animal Verified"
                            msg = "Your animal has been verified\n\n\n Animal Id:" +
                                    " ${res.body.animalId}"
                        }
                        customPopups.showAlertPopup(title, msg, this, onClickPopup)
                    } else {
                        Constants.animal = res.body.animal
                        saveImageInPref(res.body.image)

                        Log.e("UPLOADED_IMAGE_URL", res.body.image)
                        Log.e("ANIMAL_NAME", res.body.animal)
                        finish()
                    }
                } else if (res.status != null && res.status.message != null) {
                    customPopups.showAlertPopup(getString(R.string.str_alert), res.status.message, this, null)
                    Log.e("UPLOADED_IMAGE_ERROR", res.status.message)
                }
            } catch (ex: Exception) {
                customPopups.showAlertPopup(getString(R.string.str_alert), it as String, this, null)
            }
        }

        viewModel.isLoading.observe(this) {
            if(it)
                showLoader(progressHUD)
            else
                hideLoader(progressHUD)
        }
    }

    private val onClickPopup = object: CustomPopups.OnClick{
        override fun onBackPress() {
            finish()
        }
    }

    private fun saveImageInPref(image: String){
        val listImageUrls = PrefManager.loadListImageUrlsPreferences(sp)
        listImageUrls.add(image)
        PrefManager.saveListImageUrlsPreferences(listImageUrls, sp)
    }

    private fun getIntentData() {
        binding.isCompare = intent.getBooleanExtra(Constants.PreferenceKeys.compare, false)
    }

    private fun initView() {
        binding.isShowImage = false
        PrefManager.removePrefrence(sp, Constants.PreferenceKeys.imageUrl)
        progressHUD = ProgressHUD(this)
        navigateActivity(CameraActivity::class.java, this, false)
    }

    private fun setListeners() {
        tvRetake.setOnClickListener {
            navigateActivity(CameraActivity::class.java, this, false)
        }
        tvSubmit.setOnClickListener {
            viewModel.uploadImage(imageUrl, binding.isCompare!!)
        }
    }

    override fun onResume() {
        super.onResume()
        imageUrl = PrefManager.loadPreferences(sp, Constants.PreferenceKeys.imageUrl)!!
        if (imageUrl != "0") {
            binding.isShowImage = true
            ImageLoadAdapter.loadImage(ivShow, imageUrl)
        }
    }
}
