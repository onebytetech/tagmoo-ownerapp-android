package com.onebyte.tagmoo_owner.ui.login

import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import com.onebyte.tagmoo_owner.R
import com.onebyte.tagmoo_owner.databinding.ActivityLanguageBinding
import com.onebyte.tagmoo_owner.ui.base.BaseActivity
import com.onebyte.tagmoo_owner.ui.home.HomeActivity
import com.onebyte.tagmoo_owner.ui.home.OwnerHomeActivity
import com.onebyte.tagmoo_owner.utils.PrefManager
import com.onebyte.tagmoo_owner.utils.navigateActivity
import kotlinx.android.synthetic.main.activity_language.*
import java.util.*


class LanguageActivity : BaseActivity<ActivityLanguageBinding, LoginViewModel>() {
    override fun getViewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java

    override fun layoutId(): Int = R.layout.activity_language

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(!PrefManager.getLocale(sp).equals("0") && !intent.hasExtra("from"))
            navigateActivity(OwnerLoginActivity::class.java, this, true)
        setListeners()
        PrefManager.saveLocale("en", sp)
    }

    private fun setListeners() {
        tvDone.setOnClickListener{
            if(rUrdu.isChecked) {
                PrefManager.saveLocale("ur", sp)
            } else {
                PrefManager.saveLocale("en", sp)
            }
            setLocale(PrefManager.getLocale(sp))
            if(intent.hasExtra("from"))
                navigateActivity(OwnerHomeActivity::class.java, this)
            else
                navigateActivity(OwnerLoginActivity::class.java, this, true)

        }

    }

    @Suppress("DEPRECATION")
    private fun setLocale(language: String) {
        PrefManager.saveLocale(language, sp)
        val resources: Resources = resources
        val configuration: Configuration = resources.configuration
        val displayMetrics: DisplayMetrics = resources.displayMetrics
        val locale = Locale(language)
        configuration.setLocale(locale)
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            applicationContext.createConfigurationContext(configuration)
        } else {
            resources.updateConfiguration(configuration, displayMetrics)
        }
    }
}
